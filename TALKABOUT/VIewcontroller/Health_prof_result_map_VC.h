//
//  Health_prof_result_map_VC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface Health_prof_result_map_VC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,callScreenDelegate>
{
    CLLocationManager *objLocationManager;
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *tabbar;

@property (weak, nonatomic) IBOutlet UIImageView *popup_pro_img;
@property (weak, nonatomic) IBOutlet UILabel *Popup_name;
@property (weak, nonatomic) IBOutlet UILabel *popup_address;

@property (weak, nonatomic) IBOutlet UILabel *popup_distance;
- (IBAction)popup_detail_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *popup_star1;
@property (weak, nonatomic) IBOutlet UIImageView *popup_star2;
@property (weak, nonatomic) IBOutlet UIImageView *popup_star3;
@property (weak, nonatomic) IBOutlet UIImageView *popup_star4;
@property (weak, nonatomic) IBOutlet UIImageView *popup_star5;

@property (weak, nonatomic) IBOutlet UIView *popup_view;
- (IBAction)list_btn_Click:(id)sender;
- (IBAction)Filter_btn_Click:(id)sender;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;

@end

//
//  SignInVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"


@interface SignInVC : UIViewController
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
- (IBAction)Cancle_Btn_Click:(id)sender;
- (IBAction)Forgot_btn_Click:(id)sender;

- (IBAction)Check_uncheck_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Check_uncheck_Btn;
@property (weak, nonatomic) IBOutlet UITextField *EmailTF;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTF;
@property (weak, nonatomic) IBOutlet UIImageView *Check_uncheck_image;


- (IBAction)SignInBtn_Click:(id)sender;
- (IBAction)Join_Now_Btn_Click:(id)sender;

@end

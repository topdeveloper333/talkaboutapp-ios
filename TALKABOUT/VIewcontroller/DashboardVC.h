//
//  DashboardVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"

@interface DashboardVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;

@property (weak, nonatomic) IBOutlet UIButton *Menu_Btn;
@property (weak, nonatomic) IBOutlet UISearchBar *Search_Bar;
@property (weak, nonatomic) IBOutlet UIView *TopView1;
@property (weak, nonatomic) IBOutlet UIView *TopView3;
@property (weak, nonatomic) IBOutlet UIView *TopView4;
@property (weak, nonatomic) IBOutlet UIView *TopView2;
@property (weak, nonatomic) IBOutlet UIView *my_booking_View;
@property (weak, nonatomic) IBOutlet UIView *MBView1;
@property (weak, nonatomic) IBOutlet UIView *MBView2;
@property (weak, nonatomic) IBOutlet UILabel *MNView1_price_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *MBView2_price_Lbl;
@property (weak, nonatomic) IBOutlet UIImageView *MBView1_price_img;
@property (weak, nonatomic) IBOutlet UIImageView *MBView2_price_img;
@property (weak, nonatomic) IBOutlet UIImageView *mbView1_image;
@property (weak, nonatomic) IBOutlet UILabel *mbView1_booking_typ;
@property (weak, nonatomic) IBOutlet UILabel *mbView1_approw_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbView1_booking_time_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbView1_booking_date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbview1_dr_name_lbl;


@property (weak, nonatomic) IBOutlet UIImageView *mbView2_image;
@property (weak, nonatomic) IBOutlet UILabel *mbView2_booking_typ;
@property (weak, nonatomic) IBOutlet UILabel *mbView2_approw_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbView2_booking_time_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbView2_booking_date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *mbview2_dr_name_lbl;



@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;


- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *Hideview;

@property (weak, nonatomic) IBOutlet UIView *mainview;

@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;
@property (weak, nonatomic) IBOutlet UILabel *Header_title;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;

- (IBAction)ViewAll_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *searchTblView;

@end

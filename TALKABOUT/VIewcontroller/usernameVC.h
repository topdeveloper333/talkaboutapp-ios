//
//  usernameVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface usernameVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
- (IBAction)back_btn_Click:(id)sender;
- (IBAction)save_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *First_name_TF;
@property (weak, nonatomic) IBOutlet UITextField *Last_Name_TF;
@property (weak, nonatomic) IBOutlet UIView *popup_back_view;
@property (weak, nonatomic) IBOutlet UIView *popup_view;
- (IBAction)popup_save_btn_click:(id)sender;
- (IBAction)popup_dontsave_btn_Click:(id)sender;

@end

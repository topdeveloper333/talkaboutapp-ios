//
//  SideViewVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 30/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "SideViewVC.h"
#import "AccountVC.h"
#import "AppDelegate.h"
@interface SideViewVC ()
{
    AppDelegate *app;
}
@end

@implementation SideViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    self.view.backgroundColor=[UIColor clearColor];
    _Profile_Image_View.layer.cornerRadius=_Profile_Image_View.frame.size.width/2;
    _Profile_Image_View.layer.borderWidth=0.5;
    _Profile_Image_View.layer.borderColor=[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.3].CGColor;
    _Profile_Image_View.clipsToBounds=YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)account_Btn_Click:(id)sender {
    
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    AccountVC *Nextview = (AccountVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"AccountVC"];
    
    [navigationController pushViewController:Nextview animated:NO];}

- (IBAction)Setting_btn_Click:(id)sender {
}

- (IBAction)Contavt_us_Btn_Click:(id)sender {
}

- (IBAction)rate_us_btn_Click:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/find-near-friends/id1201707006?ls=1&mt=8"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)Tell_a_Frnd_btn_Click:(id)sender {
    {
        
        
            NSString * message = @"TALKAOUT";
        
        
        NSArray * shareItems = @[ message];
        
        UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        avc.excludedActivityTypes = excludeActivities;
        
        //    [self presentViewController:activityVC animated:YES completion:nil];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self presentViewController:avc animated:YES completion:nil];
        }
        //if iPad
        else {
            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:avc];
            [popup presentPopoverFromRect:CGRectMake(0,self.view.frame.size.height, self.view.frame.size.width, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
    }
}

- (IBAction)Logout_btn_Click:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to logout ?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                         {
                             [self LogOutMethod];
                         }];
    UIAlertAction* cancle = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    [alertController addAction:ok];
    [alertController addAction:cancle];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)LogOutMethod
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    NSString *emailSTR=[[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    NSLog(@"zxczx %@",emailSTR);
    [self.navigationController popToRootViewControllerAnimated:YES];
//    UINavigationController *navigationController = (UINavigationController *)appDel.window.rootViewController;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    Login_SignUpVC *MainPage = (Login_SignUpVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Login_SignUpVC"];
//    [navigationController pushViewController:MainPage animated:YES];
}

- (IBAction)Terms_btn_Click:(id)sender {
}

- (IBAction)Privacy_btn_Click:(id)sender {
}
@end

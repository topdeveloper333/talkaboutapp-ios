//
//  FilterVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 01/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "FilterVC.h"
#import "Health_prof_result_VC.h"
#import "AppDelegate.h"
@interface FilterVC ()<SINCallClientDelegate>
{
    AppDelegate *app;
    NSMutableArray *therapyType_array;
    NSMutableArray *DoctorType_array;
    NSMutableArray *Who_tack_array;
    NSMutableArray *Locationarray;

    NSString *type;
}
@end

@implementation FilterVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    _optionview.hidden=YES;
    _option_sub_view.layer.cornerRadius=10;
    _option_sub_view.clipsToBounds=YES;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_all_doctor_type) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_all_doctor_THERAPY) userInfo:nil repeats:NO];

        _takes_tf.text= @"Select Who takes You?";
        _therapy_tf.text= @"Select therapy type";
        _location_Tf.text= @"Current location";
        _what_wrong_tf.text= @"Select Doctor type";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([type isEqualToString:@"takes"]) {
        return Who_tack_array.count;
    }
    else if ([type isEqualToString:@"therapy"]) {
        return therapyType_array.count;

    }
    else if ([type isEqualToString:@"location"]) {
        return Locationarray.count;

    }
    else {
        return DoctorType_array.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidenti = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidenti];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenti];
    }
    if ([type isEqualToString:@"takes"]) {
        cell.textLabel.text= [Who_tack_array objectAtIndex:indexPath.row];
    }
    else if ([type isEqualToString:@"therapy"]) {
        cell.textLabel.text= [therapyType_array objectAtIndex:indexPath.row];
        
    }
    else if ([type isEqualToString:@"location"]) {
        cell.textLabel.text= [Locationarray objectAtIndex:indexPath.row];
        
    }
    else {
        cell.textLabel.text= [DoctorType_array objectAtIndex:indexPath.row];
    }
    [cell.textLabel setAdjustsFontSizeToFitWidth:YES];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([type isEqualToString:@"takes"]) {
        _takes_tf.text= [Who_tack_array objectAtIndex:indexPath.row];
    }
    else if ([type isEqualToString:@"therapy"]) {
        _therapy_tf.text= [therapyType_array objectAtIndex:indexPath.row];

    }
    else if ([type isEqualToString:@"location"]) {
        _location_Tf.text= [Locationarray objectAtIndex:indexPath.row];

    }
    else {
        _what_wrong_tf.text= [DoctorType_array objectAtIndex:indexPath.row];
    }
  
    _optionview.hidden=YES;

   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)applay_filter_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_prof_result_VC *Nextview = (Health_prof_result_VC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_prof_result_VC"];
    Nextview.therapy=_therapy_tf.text;
    Nextview.location=_location_Tf.text;
    Nextview.specialties=_what_wrong_tf.text;
    Nextview.type=_takes_tf.text;
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)Close_btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)cancle_btn_click:(id)sender {
    _optionview.hidden=YES;

}
- (IBAction)What_wrong_btn_Click:(id)sender {
//    tabledata=[[NSMutableArray alloc] initWithObjects:@"Allergists/Immunologists",@"Anesthesiologists",@"Cardiologists",@"Critical Care Medicine Specialists",@"Dermatologists",@"Endocrinologists",@"Emergency Medicine Specialists",@"Family Physicians",@"Gastroenterologists",@"Otolaryngologists",@"Pathologists",@"Pediatricians", nil];
    type=@"whatwrong";
    [_tableview reloadData];
    _optionview.hidden=NO;

}

- (IBAction)location_btn_Click:(id)sender {
//    tabledata=[[NSMutableArray alloc] initWithObjects:@"Washington",@"Springfield",@"Franklin",@"Greenville",@"Bristol",@"Clinton",@"Fairview",@"Salem",@"Madison",@"Georgetown",nil];
//    type=@"location";
//    [_tableview reloadData];
//    _optionview.hidden=NO;
}

- (IBAction)therapy_type_btn_Click:(id)sender {
//    tabledata=[[NSMutableArray alloc] initWithObjects:@"Acupressure",@"Acupuncture",@"Aromatherapy",@"Aurotherapy",@"Biotherapy",@"Brachytherapy",@"Cytotherapy",@"Chrysotherapy",@"Chronotherapy",@"Counseling",@"Cryotherapy",@"Electrotherapy",@"Halotherapy",@"Hippotherapy",@"Hydrotherapy",@"Ichthyotherapy",@"Massotherapy",nil];
    type=@"therapy";
    [_tableview reloadData];
    _optionview.hidden=NO;
}

- (IBAction)takes_btn_Click:(id)sender {
//    tabledata=[[NSMutableArray alloc] initWithObjects:@"Dr Mistry",@" Dr Brick Wall",@"Dr Ether",@"Dr Tranquilli ",@"Dr Comfort",@"Dr Truluck",@"Dr Love",@"Dr Fix",@"Dr Hart",@"Dr Valentine",@"Dr Everhart",@"Dr Safety R. First",@"Dr Gentle",@"Dr Popwell",@"Dr Wack",@"Dr Handler",@"The Drs. Mash",@"Dr Bonebrake",@"Dr Butcher",@"Dr Bones",@"Dr Bender",nil];
    type=@"takes";
//    [_tableview reloadData];
//    _optionview.hidden=NO;
}


////////////////////////////////////////////////////////////////////////////////////////////////
//..................................aii part .................................................//
////////////////////////////////////////////////////////////////////////////////////////////////


-(void)get_all_doctor_type
{

    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_all_doctor_type) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                     
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@therapy.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"type_id\":\"%@\"}",@"1"];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");

        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            therapyType_array=[[NSMutableArray alloc] init];
            therapyType_array=[[jsonDictionary valueForKey:@"data"] valueForKey:@"name"];
            [_tableview reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
//    [hud hide:YES];
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)get_all_doctor_THERAPY
{
   
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_all_doctor_THERAPY) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@doctor-type.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonServiceCall:URLString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            DoctorType_array=[[NSMutableArray alloc] init];
            DoctorType_array=[[jsonDictionary valueForKey:@"data"] valueForKey:@"type"];
            [_tableview reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

//
//  AppDelegate.h
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,SINClientDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic) NSString *BASEURL;//json Service link

@property (strong,nonatomic) NSString *UserId;
@property (strong,nonatomic) NSString *User_Full_Name;
@property (strong,nonatomic) NSString *User_Email;
@property (strong,nonatomic) NSString *User_Image;
@property (strong,nonatomic) NSString *User_Refferal_Code;
@property (strong,nonatomic) NSMutableArray *Booking_list_array;

@property (strong, nonatomic) id<SINClient> client;
@property (strong,nonatomic) id<SINCall> call;
@end


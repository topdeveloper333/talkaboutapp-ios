//
//  Annotation.m
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate, title, subtitle;

@end

//
//  MybookingVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface MybookingVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UIButton *Menu_Btn;
@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;


- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIView *Hideview;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *dropdownview;
@property (weak, nonatomic) IBOutlet UITableView *dropdowntable;
- (IBAction)typer_btn_click:(id)sender;
- (IBAction)date_btn_Click:(id)sender;
- (IBAction)ststus_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *DropDownVIew;
@property (weak, nonatomic) IBOutlet UITableView *DropDownTableview;

@property (weak, nonatomic) IBOutlet UILabel *Typer_view_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *Status_view_Lbl;
@property (weak, nonatomic) IBOutlet UILabel *Date_view_Lbl;


@end

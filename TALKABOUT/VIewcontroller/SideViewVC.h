//
//  SideViewVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 30/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideViewVC : UIViewController

- (IBAction)account_Btn_Click:(id)sender;
- (IBAction)Setting_btn_Click:(id)sender;
- (IBAction)Contavt_us_Btn_Click:(id)sender;
- (IBAction)rate_us_btn_Click:(id)sender;
- (IBAction)Tell_a_Frnd_btn_Click:(id)sender;
- (IBAction)Logout_btn_Click:(id)sender;
- (IBAction)Terms_btn_Click:(id)sender;
- (IBAction)Privacy_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *Profile_Image_View;
@property (weak, nonatomic) IBOutlet UILabel *Username_Lbl;



@end

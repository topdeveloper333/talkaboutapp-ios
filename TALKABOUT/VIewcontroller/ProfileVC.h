//
//  ProfileVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface ProfileVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;

@property (weak, nonatomic) IBOutlet UIButton *Menu_Btn;
@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;


@property (weak, nonatomic) IBOutlet UIButton *Change_btn;
- (IBAction)Change_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *Profile_Img_VIew;
- (IBAction)Change_profile_btn_click:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *user_name_lbl;
@property (weak, nonatomic) IBOutlet UILabel *user_add_lbl;
@property (weak, nonatomic) IBOutlet UITextField *ageTF;
- (IBAction)male_btn_click:(id)sender;
- (IBAction)female_btn_click:(id)sender;
- (IBAction)good_btn_click:(id)sender;
- (IBAction)average_btn_click:(id)sender;
- (IBAction)poor_btn_click:(id)sender;
- (IBAction)visit1_Btn_Click:(id)sender;
- (IBAction)visit1t5_btn_click:(id)sender;
- (IBAction)visitmore5_btn_click:(id)sender;
- (IBAction)yes_btn_Click:(id)sender;
- (IBAction)No_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *male_img;
@property (weak, nonatomic) IBOutlet UIImageView *female_img;
@property (weak, nonatomic) IBOutlet UIImageView *good_img;
@property (weak, nonatomic) IBOutlet UIImageView *average_img;
@property (weak, nonatomic) IBOutlet UIImageView *poor_img;
@property (weak, nonatomic) IBOutlet UIImageView *lees1_img;
@property (weak, nonatomic) IBOutlet UIImageView *v125_img;
@property (weak, nonatomic) IBOutlet UIImageView *more5_img;
@property (weak, nonatomic) IBOutlet UIImageView *yes_img;
@property (weak, nonatomic) IBOutlet UIImageView *no_img;

- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *Hideview;

@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;


- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *tab_1_btn_Click;


@end

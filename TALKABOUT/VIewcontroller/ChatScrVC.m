//
//  ChatScrVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "ChatScrVC.h"
#import <AGEmojiKeyboard/AGEmojiKeyboardView.h>
@interface ChatScrVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource,UITextFieldDelegate,SINCallClientDelegate>
{
    CGRect screenBounds;
    NSTimer *GetmsgTimer;
    
    AGEmojiKeyboardView *emojiKeyboardView;
    NSString *Messagetext;
    NSMutableArray *chatArr;
    NSMutableArray *PrivChatHistri;

}
@end

@implementation ChatScrVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
CGRect chatviewfram;
CGRect tablveViewFram;


- (void)viewDidLoad {
    [super viewDidLoad];
    emojiKeyboardView = [[AGEmojiKeyboardView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 216) dataSource:self];
    emojiKeyboardView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    emojiKeyboardView.delegate = self;
//    self.msgTF.inputView = emojiKeyboardView;
    _msgTF.delegate=self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeOpen:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    chatviewfram=CGRectMake(_bottomVIew.frame.origin.x, _bottomVIew.frame.origin.y, _bottomVIew.frame.size.width, _bottomVIew.frame.size.height);
    
    tablveViewFram=CGRectMake(_chatTblview.frame.origin.x, _chatTblview.frame.origin.y, _chatTblview.frame.size.width, _chatTblview.frame.size.height);
    
    chatArr=[[NSMutableArray alloc] init];
    PrivChatHistri=[[NSMutableArray alloc] init];

}

-(void)viewDidAppear:(BOOL)animated
{
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    userID =[prefs stringForKey:@"LoginIdStr"];
    //    [self GetComment];
    GetmsgTimer= [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(GetComment) userInfo:nil repeats:NO];
    

    [_msgTF setText:@"Write a message..."];
    [_msgTF setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [_msgTF setTextColor:[UIColor lightGrayColor]];
    
    
}


- (void)emojiKeyBoardView:(AGEmojiKeyboardView *)emojiKeyBoardView didUseEmoji:(NSString *)emoji {
//    _bottomVIew.frame=CGRectMake(0, emojiKeyboardView.frame.origin.y-_bottomVIew.frame.size.height, _bottomVIew.frame.size.width, _bottomVIew.frame.size.height);
    self.msgTF.text = [self.msgTF.text stringByAppendingString:emoji];
}

- (void)emojiKeyBoardViewDidPressBackSpace:(AGEmojiKeyboardView *)emojiKeyBoardView {

    [self.msgTF deleteBackward];


}

- (UIColor *)randomColor {
    return [UIColor colorWithRed:drand48()
                           green:drand48()
                            blue:drand48()
                           alpha:drand48()];
}

- (UIImage *)randomImage {
    CGSize size = CGSizeMake(30, 10);
    UIGraphicsBeginImageContextWithOptions(size , NO, 0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *fillColor = [self randomColor];
    CGContextSetFillColorWithColor(context, [fillColor CGColor]);
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    CGContextFillRect(context, rect);
    
    fillColor = [self randomColor];
    CGContextSetFillColorWithColor(context, [fillColor CGColor]);
    CGFloat xxx = 3;
    rect = CGRectMake(xxx, xxx, size.width - 2 * xxx, size.height - 2 * xxx);
    CGContextFillRect(context, rect);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (UIImage *)emojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView imageForSelectedCategory:(AGEmojiKeyboardViewCategoryImage)category {
    UIImage *img = [self randomImage];
    [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return img;
}

- (UIImage *)emojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView imageForNonSelectedCategory:(AGEmojiKeyboardViewCategoryImage)category {
    UIImage *img = [self randomImage];
    [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return img;
}

- (UIImage *)backSpaceButtonImageForEmojiKeyboardView:(AGEmojiKeyboardView *)emojiKeyboardView {
    UIImage *img = [self randomImage];
    [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return img;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_btn_click:(id)sender {
    [self.view endEditing:YES];
    [GetmsgTimer invalidate];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)Send_Btn_Click:(id)sender {
    [self SendComment];

}

- (IBAction)calender_btn_Click:(id)sender {
    [self.view endEditing:YES];

}

- (IBAction)camera_btn_Click:(id)sender {
    [self.view endEditing:YES];

    NSString *other1 = @"Take a photo";
    NSString *other2 = @"Choose Existing Photo";
    NSString *cancelTitle = @"Cancle";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1,other2, nil];
    [actionSheet showInView:self.view];
}

- (IBAction)imog_btn_Click:(id)sender {
    [self.view endEditing:YES];
        self.msgTF.inputView = emojiKeyboardView;
    [_msgTF becomeFirstResponder];

}

- (IBAction)language_btn_Click:(id)sender
{
    [self.view endEditing:YES];
    self.msgTF.inputView=NULL;
    [_msgTF setKeyboardType:UIKeyboardTypeDefault];
    [self.msgTF reloadInputViews];
    [_msgTF becomeFirstResponder];

}


-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex ;
{
    if(buttonIndex==0)
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    else if(buttonIndex==1)
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.sourceType= UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if(!image)image=info[UIImagePickerControllerOriginalImage];
    NSLog(@"Scaling photo");
//    _Profile_Img_VIew.image  = image;
//    NSData *imagedata = UIImageJPEGRepresentation(_Profile_Img_VIew.image, 0.25);
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:YES];
    return YES;
}
//////////////////////////
/////// chat impliment
/////////////////////////

- (void)keyboardWillChangeOpen:(NSNotification *)note
{
    //    [self TableView_Reload_Bottom ];
    NSLog(@"show");
    if ([_msgTF.text isEqual:@"Write a message..."])
    {
        _msgTF.text = @"";
        _msgTF .textColor = [UIColor blackColor];

    }
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = _bottomVIew.frame;
    //    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height + 20);
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + _bottomVIew.frame.size.height);
    
    CGRect tableviewframe=_chatTblview.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
//    tableviewframe.size.height = self.view.frame.size.height - (keyboardBounds.size.height + _bottomVIew.frame.size.height+70);
    tableviewframe.size.height = self.view.frame.size.height - (keyboardBounds.size.height + _bottomVIew.frame.size.height+_bottomVIew.frame.size.height);

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    _bottomVIew.frame = containerFrame;
    _chatTblview.frame=tableviewframe;
//    [self TableView_Reload_Bottom];
    
    //    CGPoint offset = CGPointMake(0, self.chatTableV.contentSize.height - self.chatTableV.frame.size.height+20);
    //
    //    [self.chatTableV setContentOffset:offset animated:YES];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    {
        
        
        NSLog(@"hide");
        if(_msgTF.text.length == 0)
        {
            [self commentsTextVPlaceholder];
        }
        //TextView
        
        NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
        CGRect keyboardBounds;
        [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
        keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
        
        // get a rect for the textView frame
        CGRect containerFrame = CGRectMake(0, 500, 375, 50);
        containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
        CGRect tableviewframe=_chatTblview.frame;
        
        //tableviewframe.size.height+=260;
        screenBounds = [[UIScreen mainScreen] bounds];
        tableviewframe.size.height =  self.view.frame.size.height - _bottomVIew.frame.size.height;
        
        // animations settings
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:[duration doubleValue]];
        [UIView setAnimationCurve:[curve intValue]];
        _bottomVIew.frame = chatviewfram;
        
        //set views with new info
        screenBounds = [[UIScreen mainScreen] bounds];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        //        _chatTblview.frame = CGRectMake(0, self.view.frame.origin.y + 50, width,(self.view.frame.size.height-80)-_sndmsgview.frame.size.height);
        _chatTblview.frame = tablveViewFram;
        //        [self setUpTextFieldforIphone];
        
        //commit animations
        [UIView commitAnimations];
    }
    
}
-(void)commentsTextVPlaceholder
{
    [_msgTF setText:@"Write a message..."];
    [_msgTF setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [_msgTF setTextColor:[UIColor lightGrayColor]];
}
-(void)SendComment
{
    if ([_msgTF.text isEqual:@"Write a message..."])
    {
        Messagetext = nil;
    }
    else
    {
        Messagetext = _msgTF.text;
    }
    if ([Messagetext length] == 0)
    {
        return;
    }
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    NSMutableDictionary *dir = [[NSMutableDictionary alloc]init];
    [dir setObject:Messagetext forKey:@"message"];
//    [dir setObject:userID forKey:@"sender_id"];/////////////////////////userid
    [dir setObject:@"sender" forKey:@"sender_type"];
//    [dir setObject:_Receverid forKey:@"receiver_id"];////////////////////receiverid
    [dir setObject:@"0 second ago" forKey:@"cur_time"];
//    [dir setObject:base64String forKey:@"image"];
    [dir setObject:@"yes" forKey:@"isBase64String"];
        [chatArr addObject:dir];
    NSLog(@"chatarray %@",chatArr);
    
        PrivChatHistri =[chatArr copy];
    [_chatTblview reloadData];
    //        [app.AllChatMsgArr addObject:dir];
    @try
    {
        
        //        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ShowAllDetail) userInfo:nil repeats:NO];
        [self ShowAllDetail];
    }
    @catch (NSException * e)
    {
        NSLog(@"CATCH : %@",e);
    }
    _msgTF.text = nil;
    if (chatArr.count>2)
    {
        [self TableView_Reload_Bottom];
    }
        NSLog(@"chatarray %@",chatArr);
    
    //    NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:chatArr.count-1 inSection:0];
    //    [self.chatTableV scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    //    [self TableView_Reload_Bottom];
}
-(void)TableView_Reload_Bottom
{
    if (chatArr.count>0)
    {
        [_chatTblview reloadData];
        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:([chatArr count] - 1) inSection:0];
        [_chatTblview scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [_chatTblview beginUpdates];
        [_chatTblview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:scrollIndexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        [_chatTblview endUpdates];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    //    [chatArr removeAllObjects];
    //    [PrivChatHistri removeAllObjects];
    //    [_chatTblview reloadData];
    //    [GetmsgTimer invalidate];
    //    _blockBtn.selected=NO;
    //    _text.text=@"";
    //    [self blockUserDataShow];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    //    [chatArr removeAllObjects];
    //    [PrivChatHistri removeAllObjects];
    chatArr=nil;
    [_chatTblview reloadData];
    [GetmsgTimer invalidate];
    _msgTF.text=@"";
    
}

-(void)ShowAllDetail
{
    
    
//    NSString *URLString =[NSString stringWithFormat:@"%@input_chat.php",app.ServiceLink];
//    NSString *myRequestString = [NSString stringWithFormat:@"{\"from_user_id\":\"%@\",\"to_user_id\":\"%@\",\"message\":\"%@\",\"image\":\"%@\"}",userID,_Receverid,Messagetext,base64String];
//    base64String=@"NA";
//    jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
//    NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
//    NSLog(@"input_Chat :%@",jsonDictionary);
//    if ([[jsonDictionary valueForKey:@"success"] isEqualToString:@"1"])
//    {
//        [self Send_notification];
//        isServiceActive=@"NO";
//        //            [self TableView_Reload_Bottom];
//        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(GetComment) userInfo:nil repeats:NO];
//        [self.chatTableV reloadData];
//
//    }
//    else
//    {
//    }
//    [hud hide:YES];
//    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    
}

-(void)GetComment
{
  
//        //    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        //        //Background Thread
//        //        dispatch_async(dispatch_get_main_queue(), ^(void){
//        //            //Run UI Updates
//
//        NSLog(@"receverid : %@",userID);
//        NSLog(@"senderid : %@",_Receverid);
//
//        chatArr = NULL;
//
//        NSString *URLString =[NSString stringWithFormat:@"%@output_chat.php",app.ServiceLink];
//        NSString *myRequestString = [NSString stringWithFormat:@"{\"from_user_id\":\"%@\",\"to_user_id\":\"%@\"}",userID,_Receverid];
//        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
//        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
//        NSLog(@"Output Chat :%@",jsonDictionary);
//        NSString *success=[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"success"]];
//        int successRes =[success intValue];
//        if(successRes == 1)
//        {
//            //        chatArr = [NSMutableArray arrayWithArray:[jsonDictionary valueForKey:@"posts"]];
//            //        [[NSUserDefaults standardUserDefaults] setObject:chatArr forKey:@"oldMsgArr"];
//            //                    [self ReloadTableView];
//            chatArr = [[NSMutableArray alloc] init];
//            ProfileImage = [[NSMutableArray alloc] init];
//
//            NSMutableArray *post=[jsonDictionary valueForKey:@"posts"] ;
//            for(int i=0;i<post.count;i++)
//            {
//                NSArray *array = [[[[jsonDictionary valueForKey:@"posts"] valueForKey:@"other"]objectAtIndex:i] componentsSeparatedByString:@","];
//
//                NSString *msg=[[[jsonDictionary valueForKey:@"posts"] valueForKey:@"message"]objectAtIndex:i];
//                NSString *sender=[array objectAtIndex:0];
//                NSString *rec=[array objectAtIndex:1];
//                NSString *time=[array objectAtIndex:2];
//                NSString *image=[array objectAtIndex:4];
//
//                [ProfileImage addObject:[array objectAtIndex:4]];
//                NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
//                [dir1 setObject:msg forKey:@"message"];
//                [dir1 setObject:sender forKey:@"sender_id"];/////////////////////////userid
//                [dir1 setObject:rec forKey:@"receiver_id"];////////////////////receiverid
//                [dir1 setObject:time forKey:@"cur_time"];
//                [dir1 setObject:image forKey:@"image"];
//                [dir1 setObject:@"no" forKey:@"isBase64String"];
//
//                [chatArr addObject:dir1];
//            }
//
//            //        if(app.ChatCountArr.count<chatArr.count)
//            if(PrivChatHistri.count<chatArr.count)
//            {
//                PrivChatHistri=[[NSMutableArray alloc] init];
//                PrivChatHistri=chatArr;
//                [self TableView_Reload_Bottom];
//            }
//            else
//            {
//                //            [_chatTblview reloadData];
//                //                        [self TableView_Reload_Bottom];
//            }
//            [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(GetComment) userInfo:nil repeats:NO];
//            [hud hide:YES];
//            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
//        }
//        else
//        {
//            [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(GetComment) userInfo:nil repeats:NO];
//
//            chatArr = NULL;
//            //                app.ChatCountArr = chatArr;
//            PrivChatHistri=chatArr;
//            [hud hide:YES];
//            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
//            [_chatTblview reloadData];
//        }
//        //        });
//        //    });
    }


#pragma mark - Table View ChatView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return chatArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"messagingCell";
    PTSMessagingCell * cell = (PTSMessagingCell*) [_chatTblview dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PTSMessagingCell alloc] initMessagingCellWithReuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    //    CGSize messageSize = [PTSMessagingCell messageSize:[chatArr valueForKey:@"message"][indexPath.row]];
//    //    return messageSize.height + 2*[PTSMessagingCell textMarginVertical] + 40.0f;
//    //
//    NSString *IsimageNil=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"image"][indexPath.row] ];
//    if ([IsimageNil isEqualToString:@"NA"]) {
        NSString *cellText =[NSString stringWithFormat:@"%@",[[chatArr valueForKey:@"message"] objectAtIndex:indexPath.row]];
        UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
        CGSize constraintSize = CGSizeMake(200.0, MAXFLOAT);
        CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        //    return labelSize.height + 25;
        NSLog(@"textheight %f",labelSize.height);
        return labelSize.height +20;
//
//
//    }
//    else
//    {
//        return 150;
//
//    }
    
}

-(void)configureCell:(id)cell atIndexPath:(NSIndexPath *)indexPath {
    
    PTSMessagingCell* ccell = (PTSMessagingCell*)cell;
    
    
//    NSString *Responceid=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"sender_id"][indexPath.row] ];
//
////    NSString *useridStr =[NSString stringWithFormat:@"%@",userID];
//    NSString *useridStr =[NSString stringWithFormat:@"20"];
//
//    NSString *IsimageNil=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"image"][indexPath.row] ];
//    if ([IsimageNil isEqualToString: @"NA" ]) {
//        ccell.image=NO;
//    }
//    else
//    {
//        ccell.image=YES;
//
//    }
//    if ([Responceid isEqualToString:useridStr])
//    {
//        ccell.sent = YES;
//        ccell.avatarImageView.hidden = YES;
//    }
//    else
//    {
//        ccell.sent = NO;
//
//        NSString *oldsenderid;
//        NSString *currentsenderid;
//        if (indexPath.row !=0)
//        {
//            oldsenderid=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"sender_id"][indexPath.row-1] ];
//            currentsenderid=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"sender_id"][indexPath.row] ];
//        }
//        if ([currentsenderid isEqualToString:oldsenderid])
//        {
//            ccell.avatarImageView.hidden = YES;
//            ccell.avatarImageView.layer.masksToBounds = YES;
//        }
//        else
//        {
//            ccell.avatarImageView.hidden = NO;
//            ccell.avatarImageView.layer.masksToBounds = YES;
//
////            NSData* data = [[NSData alloc] initWithBase64EncodedString:profileimagebase64 options:0];
////            ccell.avatarImageView.image=[UIImage imageWithData:data];
//            if (ccell.avatarImageView.image == nil) {
//                ccell.avatarImageView.image=[UIImage imageNamed:@"noimg"];
//            }
//            ccell.profileimagebtn.tag=indexPath.row;
//            [ccell.profileimagebtn addTarget:self action:@selector(clickProfileImage:) forControlEvents:UIControlEventTouchUpInside];
//            ccell.sent = NO;
//        }
//    }
//    if ([IsimageNil isEqualToString:@"NA"])
//    {
//        ccell.messageLabel.text = [NSString stringWithFormat:@"%@",[chatArr valueForKey:@"message"][indexPath.row]];
//        ccell.timeLabel.text = [NSString stringWithFormat:@"%@",[chatArr valueForKey:@"cur_time"][indexPath.row]];
//
//        ccell.sendImage.hidden=YES;
//
//    }
//    else
//    {
//        NSString *isbase64string=[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"isBase64String"][indexPath.row] ];
//        ccell.sendImage.layer.cornerRadius=10.f;
//        ccell.sendImage.clipsToBounds=YES;
//        if ([isbase64string isEqualToString:@"yes"]) {
//            ccell.sendImage.hidden=NO;
//            ccell.messageLabel.text =@"";
//
//        }
//        else
//        {
//            ccell.sendImage.hidden=NO;
//            ccell.messageLabel.text =@"";
//
//            NSString *imagestring=[[chatArr valueForKey:@"image"]objectAtIndex:indexPath.row];
//
//            ccell.timeLabel.text = [NSString stringWithFormat:@"%@",[chatArr valueForKey:@"cur_time"][indexPath.row]];
//        }
//    }

    NSString *Responceid=@"26";
    NSLog(@"%@",Responceid);
    NSLog(@"%ld",(long)indexPath.row);
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[chatArr valueForKey:@"message"][indexPath.row]]);
    NSString *useridStr =@"25";
    
//    if ([Responceid isEqualToString:useridStr])
    if (indexPath.row %2==0)

    {
        ccell.sent = YES;
        ccell.avatarImageView.hidden = YES;
    }
    else
    {
        ccell.avatarImageView.hidden = NO;
        ccell.avatarImageView.layer.cornerRadius = ccell.avatarImageView.frame.size.width/2;
        ccell.avatarImageView.layer.masksToBounds = YES;
        //        ccell.avatarImageView.imageURL = [NSURL URLWithString:_UserProfile];
        ccell.sent = NO;
    }
    
    ccell.messageLabel.text = [NSString stringWithFormat:@"%@",[chatArr valueForKey:@"message"][indexPath.row]];
}
@end

//
//  newbookingVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "newbookingVC.h"
#import "AppDelegate.h"
#import "ConfirmBookingVC.h"
@interface newbookingVC ()<SINCallClientDelegate>
{
    NSMutableDictionary *_eventsByDate;
    AppDelegate *app;
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    NSMutableArray *tabledata;
    NSDate *_dateSelected;
    NSMutableArray *Fulldate;
    NSDate *SELECTEDDATE;
    NSArray* timearray;
}
@end

@implementation newbookingVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _Time_View.hidden=YES;
    _lbl.text=@"Choose a date";
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    _containerView.layer.cornerRadius=5.0;
    _containerView.clipsToBounds=YES;
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    timearray=[[NSArray alloc] initWithObjects:@"10",@"11",@"12",@"1",@"2",@"3",@"4",@"5", nil];
    if ([_Booking_type isEqualToString:@"MESSAGE"]) {
        _booking_type_title.text=@"Message";
        _bokking_type_image.image=[UIImage imageNamed:@"New_booking_message.png"];
        
    }
    else if ([_Booking_type isEqualToString:@"FACETOFACE"]) {
        _booking_type_title.text=@"Face to face";
        _bokking_type_image.image=[UIImage imageNamed:@"NewBooking_facetoface.png"];
    }
    else if ([_Booking_type isEqualToString:@"VIDEO"]) {
        _booking_type_title.text=@"Video call";
        _bokking_type_image.image=[UIImage imageNamed:@"new_booking_videocall.png"];
    }
    else  {
        _booking_type_title.text=@"Phone call";
//        _bokking_type_image.image=[UIImage imageNamed:@"con_book_phone-icn.png"];
    }
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_full_date) userInfo:nil repeats:NO];

//    // Generate random events sort by date using a dateformatter for the demonstration
//    [self createRandomEvents];
//
//    // Create a min and max date for limit the calendar, optional
//    [self createMinAndMaxDate];
//
//    [_calendarManager setMenuView:_calendarMenuView];
//    [_calendarManager setContentView:_calendarContentView];
//    [_calendarManager setDate:_todayDate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didGoTodayTouch
{
    [_calendarManager setDate:_todayDate];
}

- (IBAction)didChangeModeTouch
{
    _calendarManager.settings.weekModeEnabled = !_calendarManager.settings.weekModeEnabled;
    [_calendarManager reload];
    
    CGFloat newHeight = 300;
    if(_calendarManager.settings.weekModeEnabled){
        newHeight = 85.;
    }
    
    self.calendarContentViewHeight.constant = newHeight;
    [self.view layoutIfNeeded];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    // Other month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.backgroundColor=[UIColor greenColor];
        dayView.dotView.hidden = NO;
    }
}
- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    NSLog(@"%@",dayView.date);
    SELECTEDDATE=dayView.date;
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_Selected_date_time) userInfo:nil repeats:NO];
 
//    _Time_View.hidden=NO;
//    _lbl.text=@"Choose a time";
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    NSLog(@"Previous page loaded");
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-100];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:100];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < Fulldate.count; ++i){
        // Generate 30 random dates between now and 60 days later
//        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
//        NSString *dateString = [Fulldate objectAtIndex:i];
//        NSDateFormatter *format = [[NSDateFormatter alloc] init];
//        [format setDateFormat:@"yyyy-MM-dd"];
//        NSDate *date = [format dateFromString:dateString];
//        [format setDateFormat:@"MM-dd-yyyy"];
//        NSDate *randomDate = [format dateFromString:date];
        
        NSString *dateString =  [Fulldate objectAtIndex:i];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *randomDate = [dateFormatter dateFromString:dateString];
//        NSDate *randomDate = [NSDate date];

        NSLog(@"current date %@",[NSDate date]);
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tabledata.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidenti = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidenti];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenti];
    }
    
    
    UILabel *time=(UILabel *)[cell viewWithTag:100];
    UILabel *availability=(UILabel *)[cell viewWithTag:101];
    UIView *dotedview=(UIView *)[cell viewWithTag:102];

    dotedview.layer.cornerRadius=dotedview.frame.size.width/2;
    
    if (indexPath.row==0)
    {
        NSString * status=[tabledata valueForKey:@"10"];
        time.text=@"10:00";

        if ([status isEqualToString:@"Not availabel"])
        {
        availability.text=@"Time unavailable";
        dotedview.backgroundColor=[UIColor redColor];
        return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else if (indexPath.row==1)
        {
            NSString * status=[tabledata valueForKey:@"11"];
            time.text=@"11:00";
            
            if ([status isEqualToString:@"Not availabel"])
            {
                availability.text=@"Time unavailable";
                dotedview.backgroundColor=[UIColor redColor];
                return cell;
            }
            else
            {
                availability.text=@"Time available";
                dotedview.backgroundColor=[UIColor greenColor];
                return cell;
            }
        }
    else if (indexPath.row==2)
    {
        NSString * status=[tabledata valueForKey:@"12"];
        time.text=@"12:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else if (indexPath.row==3)
    {
        NSString * status=[tabledata valueForKey:@"1"];
        time.text=@"01:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else if (indexPath.row==4)
    {
        NSString * status=[tabledata valueForKey:@"2"];
        time.text=@"02:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else if (indexPath.row==5)
    {
        NSString * status=[tabledata valueForKey:@"3"];
        time.text=@"03:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else if (indexPath.row==6)
    {
        NSString * status=[tabledata valueForKey:@"4"];
        time.text=@"04:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    else
    {
        NSString * status=[tabledata valueForKey:@"5"];
        time.text=@"05:00";
        
        if ([status isEqualToString:@"Not availabel"])
        {
            availability.text=@"Time unavailable";
            dotedview.backgroundColor=[UIColor redColor];
            return cell;
        }
        else
        {
            availability.text=@"Time available";
            dotedview.backgroundColor=[UIColor greenColor];
            return cell;
        }
    }
    
//    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ConfirmBookingVC *Nextview = (ConfirmBookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ConfirmBookingVC"];
    Nextview.DR_ID=_Booking_doctor_id;
    Nextview.Booking_type=_Booking_type;
    Nextview.Booking_Date=[dateFormatter stringFromDate:SELECTEDDATE];
    Nextview.Booking_Time=[timearray objectAtIndex:indexPath.row];
    [navigationController pushViewController:Nextview animated:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Cancle_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)get_full_date
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_full_date) userInfo:nil repeats:NO];
                                       
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@get_bookingdate.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"doctor_id\":\"%@\"}",_Booking_doctor_id];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        Fulldate=[[NSMutableArray alloc] init];
        if (success == 1)
        {
            NSLog(@"%@",[[jsonDictionary valueForKey:@"posts"] objectAtIndex:0]);
            Fulldate=[[jsonDictionary valueForKey:@"posts"] mutableCopy];
            // Generate random events sort by date using a dateformatter for the demonstration
            [self createRandomEvents];
            
            // Create a min and max date for limit the calendar, optional
            [self createMinAndMaxDate];
            
            [_calendarManager setMenuView:_calendarMenuView];
            [_calendarManager setContentView:_calendarContentView];
            [_calendarManager setDate:_todayDate];
        }
        else
        {
            // Generate random events sort by date using a dateformatter for the demonstration
            [self createRandomEvents];
            
            // Create a min and max date for limit the calendar, optional
            [self createMinAndMaxDate];
            
            [_calendarManager setMenuView:_calendarMenuView];
            [_calendarManager setContentView:_calendarContentView];
            [_calendarManager setDate:_todayDate];
//            UIAlertController * alert = [UIAlertController
//                                         alertControllerWithTitle:@""
//                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
//                                         preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//            UIAlertAction* OkButton = [UIAlertAction
//                                       actionWithTitle:@"OK"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action) {
//
//                                       }];
//
//            [alert addAction:OkButton];
//            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)get_Selected_date_time
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_Selected_date_time) userInfo:nil repeats:NO];
                                       
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *finalDate = [dateFormatter stringFromDate:SELECTEDDATE];
        NSLog(@"final date :: %@",finalDate);
        NSString *URLString =[NSString stringWithFormat:@"%@get_bookingtime.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"book_date\":\"%@\"}",finalDate];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        Fulldate=[[NSMutableArray alloc] init];
        if (success == 1)
        {
            _Time_View.hidden=NO;
            _lbl.text=@"Choose a time";
            NSLog(@"%@",[[jsonDictionary valueForKey:@"posts"]valueForKey:@"1"]);
            tabledata=[[jsonDictionary valueForKey:@"posts"] mutableCopy];
            [_Time_tbl_view reloadData];
        }
        else
        {
           
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

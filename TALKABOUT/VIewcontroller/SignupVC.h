//
//  SignupVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"

@interface SignupVC : UIViewController
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
- (IBAction)Back_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *Full_name_TF;
@property (weak, nonatomic) IBOutlet UITextField *Email_Tf;
@property (weak, nonatomic) IBOutlet UITextField *pswd_TF;
@property (weak, nonatomic) IBOutlet UITextField *c_pswd_TF;
@property (weak, nonatomic) IBOutlet UITextField *R_Code_TF;
- (IBAction)SignUp_Btn_Click:(id)sender;
- (IBAction)Sign_In_Btn_Click:(id)sender;



@end

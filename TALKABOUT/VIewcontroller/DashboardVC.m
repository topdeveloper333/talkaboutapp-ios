//
//  DashboardVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "DashboardVC.h"
#import "SWRevealViewController.h"
#import "HealthLibariansVC.h"
#import "AppDelegate.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "SideViewVC.h"
#import "Health_prof_result_VC.h"
#import "FilterVC.h"
#import <Sinch/Sinch.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface DashboardVC ()<SINCallClientDelegate>
{
    AppDelegate *app;
    CGRect menu_btn_frm;
    CGRect Header_title_frm;
    NSArray *searchResults,*tableData;
    UIButton *filter_btn;

}
@end

@implementation DashboardVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    filter_btn=[self.view viewWithTag:121];
    [filter_btn addTarget:self action:@selector(filter_btn_Click) forControlEvents:UIControlEventTouchUpInside];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_book_doctor) userInfo:nil repeats:NO];
    _searchTblView.hidden=YES;
        _Tabbar_View.backgroundColor=[UIColor whiteColor];
    
    tableData=[[NSArray alloc] initWithObjects:@"1",@"2",@"3", nil];
//    self.Search_Bar.layer.borderWidth = 2.0;
//    self.Search_Bar.layer.borderColor = [UIColor brownColor].CGColor;
    self.Search_Bar.layer.cornerRadius = 5.0;
    self.Search_Bar.barTintColor = [UIColor colorWithRed:255/255.0 green:246/255.0 blue:241/255.0 alpha:1.0];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor blackColor]];

    self.Search_Bar.backgroundColor = [UIColor clearColor];
    
//    UITextField *textField = [self.Search_Bar valueForKey:@"_searchField"];
//    textField.textColor = [UIColor colorWithRed:.90 green:1.0 blue:1.0 alpha:1];
//    textField.placeholder = @"Search Health professional";
//    textField.textAlignment=NSTextAlignmentCenter;
////    textField.leftViewMode = UITextFieldViewModeNever; //hiding left view
//    textField.backgroundColor = [UIColor colorWithRed:255/255.0 green:246/255.0 blue:241/255.0 alpha:1.0];
//    textField.font = [UIFont systemFontOfSize:12];
////    [textField setValue:[UIColor brownColor] forKeyPath:@"_placeholderLabel.textColor"];
//    
//    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 30)];
//    imgview.image = [UIImage imageNamed:@"search.png"]; //you need to set search icon for textfield's righ view
//    
//    textField.rightView = imgview;
//    textField.rightViewMode = UITextFieldViewModeAlways;
//    _Search_Bar.clipsToBounds=YES;
    

        [self addshadow:_MBView1];
        [self addshadow:_MBView2];
        [self addshadow:_TopView1];
        [self addshadow:_TopView2];
        [self addshadow:_TopView3];
        [self addshadow:_TopView4];
        [self addshadow:_Tabbar_View];
        menu_btn_frm=CGRectMake(_Menu_Btn.frame.origin.x, _Menu_Btn.frame.origin.y, _Menu_Btn.frame.size.width, _Menu_Btn.frame.size.height);
        Header_title_frm=CGRectMake(_Header_title.frame.origin.x, _Header_title.frame.origin.y, _Header_title.frame.size.width, _Header_title.frame.size.height);
    
        _MBView2_price_Lbl.layer.cornerRadius=_MBView2_price_Lbl.frame.size.height/2;
        _MBView2_price_Lbl.clipsToBounds=YES;

        _MNView1_price_Lbl.layer.cornerRadius=_MNView1_price_Lbl.frame.size.height/2;
        _MNView1_price_Lbl.clipsToBounds=YES;

        _MBView1_price_img.layer.cornerRadius=_MBView1_price_img.frame.size.width/2;
        _MBView2_price_img.layer.cornerRadius=_MBView2_price_img.frame.size.width/2;
        _MBView1_price_img.clipsToBounds=YES;
        _MBView2_price_img.clipsToBounds=YES;

    _Sideview.hidden=YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
    
    sb.view.backgroundColor = [UIColor clearColor];
    [sb willMoveToParentViewController:self];
    
    [self.SubSIdeVIew addSubview:sb.view];
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
    
   
    
}
-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _Menu_Btn.frame=menu_btn_frm;
//    [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}
-(void)filter_btn_Click
{
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    FilterVC *Nextview = (FilterVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"FilterVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)menu_Btn_Click:(id)sender {
    if (_Sideview.hidden==YES) {
        
    _Sideview.hidden=NO;
    self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
        _Menu_Btn.frame=CGRectMake(_Menu_Btn.frame.origin.x,_Menu_Btn.frame.origin.y, 30, 30);
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=YES;

        
    }
    else
    {
        _Sideview.hidden=YES;
        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _Menu_Btn.frame=menu_btn_frm;
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=NO;




    }
}

////////////////////
//////search bar
///////////////////

#pragma mark - search
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
    NSPredicate *resultPredicate    = [NSPredicate predicateWithFormat:@"self contains[c] %@", searchText];
    searchResults  = [tableData filteredArrayUsingPredicate:resultPredicate];
    [_searchTblView reloadData];
    _searchTblView.hidden=NO;


}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    // user did type something, check our datasource for text that looks the same
    if (searchText.length>0) {
        // search and reload data source
        [self filterContentForSearchText:searchText
                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                          objectAtIndex:[self.searchDisplayController.searchBar
                                                         selectedScopeButtonIndex]]];
        [_searchTblView reloadData];
        _searchTblView.hidden=NO;


    }else{
        searchResults=tableData;
        [_searchTblView reloadData];
        _searchTblView.hidden=NO;

        // if text lenght == 0
        // we will consider the searchbar is not active
        
    }
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self cancelSearching];
    _searchTblView.hidden=YES;

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
    _searchTblView.hidden=YES;

}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    // we used here to set self.searchBarActive = YES
    // but we'll not do that any more... it made problems
    // it's better to set self.searchBarActive = YES when user typed something
    [searchBar setShowsCancelButton:YES animated:YES];
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    // this method is being called when search btn in the keyboard tapped
    // we set searchBarActive = NO
    // but no need to reloadCollectionView
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.view endEditing:YES];
    _searchTblView.hidden=YES;

}
-(void)cancelSearching{
    
    [_Search_Bar resignFirstResponder];
    _Search_Bar.text  = @"";
    [self.view endEditing:YES];

    _searchTblView.hidden=YES;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self.view endEditing:YES];
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_prof_result_VC *Nextview = (Health_prof_result_VC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_prof_result_VC"];
    
    [navigationController pushViewController:Nextview animated:NO];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
   
    cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return searchResults.count;
}


- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)ViewAll_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    
}
-(void)Get_all_book_doctor
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_book_doctor) userInfo:nil repeats:NO];
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@get_bookinglist.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",app.UserId];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        app.Booking_list_array=[[NSMutableArray alloc] init];
        if (success == 1)
        {
            app.Booking_list_array=[jsonDictionary valueForKey:@"data"];
            if (app.Booking_list_array.count==1) {
                NSURL *imageurl=[NSURL URLWithString:[[app.Booking_list_array valueForKey:@"image"] objectAtIndex:0]];
                [_mbView1_image sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
                _mbView1_booking_date_lbl.text=[[app.Booking_list_array valueForKey:@"booking_date"] objectAtIndex:0];
                _mbView1_booking_time_lbl.text=[[app.Booking_list_array valueForKey:@"booking_time"] objectAtIndex:0];
                _mbview1_dr_name_lbl.text=[[app.Booking_list_array valueForKey:@"name"] objectAtIndex:0];
                _mbView1_approw_lbl.text=@"Approwed";
                _mbView1_booking_typ.text=[[app.Booking_list_array valueForKey:@"type"] objectAtIndex:0];
                _MNView1_price_Lbl.text=[NSString stringWithFormat:@"$%@",[[app.Booking_list_array valueForKey:@"charge"] objectAtIndex:0]];
                _MBView1.hidden=YES;
            }
            else if (app.Booking_list_array.count>1)
            {

                
                NSURL *imageurl=[NSURL URLWithString:[[app.Booking_list_array valueForKey:@"image"] objectAtIndex:0]];
                [_mbView1_image sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
                _mbView1_booking_date_lbl.text=[[app.Booking_list_array valueForKey:@"booking_date"] objectAtIndex:0];
                _mbView1_booking_time_lbl.text=[[app.Booking_list_array valueForKey:@"booking_time"] objectAtIndex:0];
                _mbview1_dr_name_lbl.text=[[app.Booking_list_array valueForKey:@"name"] objectAtIndex:0];
                _mbView1_approw_lbl.text=@"Approwed";
                _mbView1_booking_typ.text=[[app.Booking_list_array valueForKey:@"type"] objectAtIndex:0];
                _MNView1_price_Lbl.text=[NSString stringWithFormat:@"$%@",[[app.Booking_list_array valueForKey:@"charge"] objectAtIndex:0]];
                

                NSURL *imageurl1=[NSURL URLWithString:[[app.Booking_list_array valueForKey:@"image"] objectAtIndex:1]];
                [_mbView2_image sd_setImageWithURL:imageurl1 placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
                _mbView2_booking_date_lbl.text=[[app.Booking_list_array valueForKey:@"booking_date"] objectAtIndex:1];
                _mbView2_booking_time_lbl.text=[[app.Booking_list_array valueForKey:@"booking_time"] objectAtIndex:1];
                _mbview2_dr_name_lbl.text=[[app.Booking_list_array valueForKey:@"name"] objectAtIndex:1];
                _mbView2_approw_lbl.text=@"Approwed";
                _mbView2_booking_typ.text=[[app.Booking_list_array valueForKey:@"type"] objectAtIndex:1];
                _MBView2_price_Lbl.text=[NSString stringWithFormat:@"$%@",[[app.Booking_list_array valueForKey:@"charge"] objectAtIndex:1]];
                
            }
            else
            {
                
                _mbView1_image.hidden=YES;;
                _mbView1_booking_date_lbl.hidden=YES;;
                _mbView1_booking_time_lbl.hidden=YES;;
                _mbview1_dr_name_lbl.text=@"No booking availabel";;
                _mbView1_approw_lbl.hidden=YES;;
                _mbView1_booking_typ.hidden=YES;;
                _MNView1_price_Lbl.hidden=YES;
                _MBView1.hidden=YES;
                _MBView2.hidden=YES;
            }
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

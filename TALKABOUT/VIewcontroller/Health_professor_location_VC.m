//
//  Health_professor_location_VC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "Health_professor_location_VC.h"
#import "Annotation.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "AppDelegate.h"
#import "SideViewVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "JPSThumbnailAnnotation.h"


@interface Health_professor_location_VC ()<SINCallClientDelegate>
{
    AppDelegate *app;
    CGRect menu_btn_frm;

}

@end

@implementation Health_professor_location_VC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self addshadow:_Tabview];
    _Tabview.backgroundColor=[UIColor whiteColor];
  
    self.Sideview.hidden=YES;
    menu_btn_frm=CGRectMake(_menu_btn.frame.origin.x, _menu_btn.frame.origin.y, _menu_btn.frame.size.width, _menu_btn.frame.size.height);
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
//    sb.view.backgroundColor = [UIColor clearColor];
//    [sb willMoveToParentViewController:self];
//    
//    [self.sideSubview addSubview:sb.view];
//    
//    [self addChildViewController:sb];
//    [sb didMoveToParentViewController:self];
    
    [_mapview setDelegate:self];
    _mapview.delegate=self;
    _mapview.showsUserLocation=YES;
    [self getcurrentlocation];

    
//    double latitude = 22.3039;
//    double longitude = 70.8022;
    
    NSLog(@"cut lat: %@", _Latitude);
    NSLog(@"cut lon: %@", _longitude);
    double latitude = [_Latitude doubleValue];
    double longitude = [_longitude doubleValue];


    
//    CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(latitude, longitude);
//
//    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
//    [annotation setCoordinate:coord1];
//    annotation.title=@"hello";
//
//    [self.mapview addAnnotation:annotation];
    
    JPSThumbnail *thumbnail = [[JPSThumbnail alloc] init];
    thumbnail.image = [UIImage imageNamed:@"Health_pro_pin.png"];
//    thumbnail.imageprofile=[UIImage imageNamed:@"123.jpg"];
  
    NSString *strSub = [_popup_image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strSub]];
    NSData *data = [NSData dataWithContentsOfURL:imgUrl];
    UIImage *img = [[UIImage alloc] initWithData:data];
    thumbnail.imageprofile=img;

    thumbnail.title = _popup_dr_name;
    thumbnail.subtitle = _popup_dr_type;
    thumbnail.address=_popup_dr_address;
    int starvalue=[_popup_dr_star integerValue];
    if(starvalue == 1)
    {
    thumbnail.star1=[UIImage imageNamed:@"selected-star.png"];
    thumbnail.star2=[UIImage imageNamed:@"Diselect-star.png"];
    thumbnail.star3=[UIImage imageNamed:@"Diselect-star.png"];
    thumbnail.star4=[UIImage imageNamed:@"Diselect-star.png"];
    thumbnail.star5=[UIImage imageNamed:@"Diselect-star.png"];
    }
    else if(starvalue == 2)
    {
        thumbnail.star1=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star2=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star3=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star4=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star5=[UIImage imageNamed:@"Diselect-star.png"];
    }
    else if(starvalue == 3)
    {
        thumbnail.star1=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star2=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star3=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star4=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star5=[UIImage imageNamed:@"Diselect-star.png"];
    }
    else if(starvalue == 4)
    {
        thumbnail.star1=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star2=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star3=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star4=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star5=[UIImage imageNamed:@"Diselect-star.png"];
    }
    else if(starvalue == 5)
    {
        thumbnail.star1=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star2=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star3=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star4=[UIImage imageNamed:@"selected-star.png"];
        thumbnail.star5=[UIImage imageNamed:@"selected-star.png"];
    }
    else
    {
        thumbnail.star1=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star2=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star3=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star4=[UIImage imageNamed:@"Diselect-star.png"];
        thumbnail.star5=[UIImage imageNamed:@"Diselect-star.png"];
    }
    thumbnail.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    thumbnail.disclosureBlock = ^{ NSLog(@"selected Empire"); };
    
    [_mapview addAnnotation:[JPSThumbnailAnnotation annotationWithThumbnail:thumbnail]];
    
    _location_popup_View.hidden=YES;

}
-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _menu_btn.frame=menu_btn_frm;
//    [self.menu_btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
-(void)getcurrentlocation
{
    objLocationManager = [[CLLocationManager alloc] init];
    objLocationManager.delegate = self;
    objLocationManager.distanceFilter = kCLDistanceFilterNone;
    objLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    if ([objLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [objLocationManager requestWhenInUseAuthorization];
    }
    [objLocationManager startUpdatingLocation];
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
//    location.latitude = userLocation.coordinate.latitude;
//    location.longitude = userLocation.coordinate.longitude;
    location.latitude = [_Latitude doubleValue];
    location.longitude = [_longitude doubleValue];
    region.span = span;
    region.center = location;
    [mapView setRegion:region animated:YES];
}
//-(MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
//{
//
////    MKAnnotationView *MyPin = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"current"];
////
////
////    UIView *mainview=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 30, 50 )];
////    mainview.backgroundColor=[UIColor clearColor];
////    mainview.clipsToBounds=YES;
////
////    UIImageView *back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
////    back_img.image=[UIImage imageNamed:@"active-pin.png"];
////    back_img.contentMode=UIViewContentModeScaleAspectFit;
////    [mainview addSubview:back_img];
////
////    //    MKPinAnnotationView *MyPin=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"current"];
////    //MyPin.pinColor = MKPinAnnotationColorPurple;
////
//////    UIButton *advertButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//////    [advertButton addTarget:self action:@selector(button) forControlEvents:UIControlEventTouchUpInside];
//////
//////    MyPin.rightCalloutAccessoryView = advertButton;
//////     MyPin.draggable = YES;
////
////
////    [MyPin addSubview:mainview];
//////     MyPin.animatesDrop=TRUE;
//////     MyPin.canShowCallout = YES;
////    MyPin.highlighted = NO;
////
////    return MyPin;
//
//
//
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
//
//    static NSString *reuseId = @"reuseid";
//    MKAnnotationView *av = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
//    if (av == nil)
//    {
//        av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId] ;
//
//        UIView *mainview=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 30, 50 )];
//        mainview.backgroundColor=[UIColor clearColor];
//        mainview.clipsToBounds=YES;
//
//        UIImageView *back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
//        back_img.image=[UIImage imageNamed:@"active-pin.png"];
//        back_img.contentMode=UIViewContentModeScaleAspectFit;
//        [mainview addSubview:back_img];
//
//        [av addSubview:mainview];
//
//        //Following lets the callout still work if you tap on the label...
//        av.canShowCallout = NO;
//        av.frame = mainview.frame;
//    }
//    else
//    {
//        av.annotation = annotation;
//    }
//
//
//    return av;
//}



//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
//{
//        _location_popup_View.hidden=NO;
//        [view addSubview:_location_popup_View];
//
//
//
//}
//
//- (void)mapView:(MKMapView *)aMapView didDeselectAnnotationView:(MKAnnotationView *)view
//{
//
//}

- (void)dealloc {
    _mapview.delegate=nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)button
{
    NSLog(@"say hello...");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)menu_btn_Click:(id)sender {
//    if (_Sideview.hidden==YES) {
//
//        _Sideview.hidden=NO;
//        self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
//        _menu_btn.frame=CGRectMake(_menu_btn.frame.origin.x,_menu_btn.frame.origin.y, 30, 30);
//        [self.menu_btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
//        _Hader_back_img.hidden=YES;
//    }
//    else
//    {
//        _Sideview.hidden=YES;
//        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        _menu_btn.frame=menu_btn_frm;
//        [self.menu_btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//        _Hader_back_img.hidden=NO;
//    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}


//-(void)addAllPins
//{
//
//    arrCoordinateStr = [[NSMutableArray alloc] initWithCapacity:NameOfUser.count];
//
//    for ( l=0; l<Userlocation.count; l++)
//    {
//        [arrCoordinateStr addObject:[Userlocation objectAtIndex:l]];
//    }
//
//    map.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//    [self.MapView addSubview:map];
//
//    for( i = 0; i < NameOfUser.count; i++)
//    {
//        [map addAnnotations:[self AddAnotataion]];
//        NSString *UrlStr=[UserPic objectAtIndex:i];
//        //        ImageData=[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",UrlStr]]];
//        NSString *urldata=@"http://www.smartbaba.in/getbusiness.in/findmyfriend/webapi/profile-img.png";
//        ImageData=[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urldata]]];
//
//    }
//
//    [indicator stopAnimating];
//
//    [hud dismissAnimated:YES];
//
//}
//- (void) getAddressFromLatLon:(CLLocation *)bestLocation
//{
//    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
//    [geocoder reverseGeocodeLocation:bestLocation
//                   completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if (error){
//             NSLog(@"Geocode failed with error: %@", error);
//             return;
//         }
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
//         NSLog(@"locality %@",placemark.locality);
//         NSLog(@"postalCode %@",placemark.postalCode);
//
//     }];
//
//}

//-(NSArray *)AddAnotataion
//{
//    JPSThumbnail *empire = [[JPSThumbnail alloc] init];
//    empire.image = [UIImage imageWithData:ImageData];
//    empire.title =UserName[i];
//
//    empire.subtitle = UserAdd[i];
//    NSString *theString =[arrCoordinateStr objectAtIndex:i];
//    NSArray *items = [theString componentsSeparatedByString:@","];
//    NSString *item1,*item2;
//    if (items.count > 1) {
//        item1=[items objectAtIndex:0];
//        item2=[items objectAtIndex:1];
//    }
//    else {
//        item1=[items objectAtIndex:0];
//    }
//
//    double first =[item1 doubleValue];
//    double sec =[item2 doubleValue];
//    //    NSLog(@"%@",theString);
//
//    empire.coordinate = CLLocationCoordinate2DMake(first, sec);
//    empire.disclosureBlock = ^{ NSLog(@"selected Appple%d",i); };
//    empire.msgRcvId = [userId objectAtIndex:i];
//    //    NSLog(@"%@",[UserPic objectAtIndex:i]);
//    empire.msgProPic = [UserPic objectAtIndex:i];
//
//    empire.msgName = NameOfUser[i];
//    empire.msContact = UserContact[i];
//
//    return @[[JPSThumbnailAnnotation annotationWithThumbnail:empire]];
//}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)])
    {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}

@end

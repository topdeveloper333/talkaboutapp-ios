//
//  Health_prof_result_VC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"


@interface Health_prof_result_VC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)map_btn_Click:(id)sender;
- (IBAction)Filter_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *tabbar;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;

@property (weak, nonatomic) NSString *location;
@property (weak, nonatomic) NSString *therapy;
@property (weak, nonatomic) NSString *specialties;
@property (weak, nonatomic) NSString *type;


@end

//
//  Health_library_detailVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 04/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface Health_library_detailVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
- (IBAction)Back_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *Title_lbl;
@property (weak, nonatomic) IBOutlet UITextView *Detail_TextView;

@property (weak, nonatomic) NSString *titleText;
@property (weak, nonatomic) NSString *Description_text;

@end

//
//  SignInVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "SignInVC.h"
#import "AppDelegate.h"
#import "SignupVC.h"
#import "DashboardVC.h"
#import "SWRevealViewController.h"
@interface SignInVC ()<UITextFieldDelegate>
{
    AppDelegate *app;
}
@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];

    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    _EmailTF.delegate=self;
    _EmailTF.backgroundColor=[UIColor clearColor];
    _EmailTF.layer.cornerRadius=5.0f;
    _EmailTF.layer.borderWidth=1.0f;
    _EmailTF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _EmailTF.clipsToBounds=YES;
    
    _PasswordTF.delegate=self;
    _PasswordTF.backgroundColor=[UIColor clearColor];
    _PasswordTF.layer.cornerRadius=5.0f;
    _PasswordTF.layer.borderWidth=1.0f;
    _PasswordTF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _PasswordTF.clipsToBounds=YES;
    
    _EmailTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _PasswordTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    
    _EmailTF.text=@"asd@gmail.com";
    _PasswordTF.text=@"asd";
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}
- (IBAction)Cancle_Btn_Click:(id)sender {
}

- (IBAction)Forgot_btn_Click:(id)sender {
}

- (IBAction)Check_uncheck_Btn_Click:(id)sender {
    if (_Check_uncheck_Btn.selected==YES)
    {
        _Check_uncheck_Btn.selected=NO;
        _Check_uncheck_image.image=[UIImage imageNamed:@"uncheck.png"];
    }
    else
    {
        _Check_uncheck_Btn.selected=YES;
        _Check_uncheck_image.image=[UIImage imageNamed:@"check.png"];
    }
}
- (IBAction)SignInBtn_Click:(id)sender {
    [self.view endEditing:YES];
    if ([_EmailTF.text isEqualToString:@""]) {

        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter Proper Email id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if([_PasswordTF.text isEqualToString:@""])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter confirm password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(signin_api_call) userInfo:nil repeats:NO];

    }
}

- (IBAction)Join_Now_Btn_Click:(id)sender {
  
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SignupVC *Nextview = (SignupVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"SignupVC"];
    
    [navigationController pushViewController:Nextview animated:YES];
}

-(void)signin_api_call
{
    //    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    //    reach.reachableBlock = ^(Reachability*reach)
    //    {
    //
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            NSLog(@"REACHABLE!");
    //        });
    //    };
    //
    //    reach.unreachableBlock = ^(Reachability*reach)
    //    {
    //        NSLog(@"UNREACHABLE!");
    //    };
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(signin_api_call) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                      
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@login.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"email\":\"%@\",\"password\":\"%@\"}",_EmailTF.text,_PasswordTF.text];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];

        if (success == 1)
        {
            
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isloginuser"];
            app.UserId=[[jsonDictionary valueForKey:@"data"] valueForKey:@"user_id"];
            app.User_Full_Name=[[jsonDictionary valueForKey:@"data"] valueForKey:@"full_name"];
            app.User_Email=[[jsonDictionary valueForKey:@"data"] valueForKey:@"email"];
            app.User_Image=[[jsonDictionary valueForKey:@"data"] valueForKey:@"img"];
            app.User_Refferal_Code=[[jsonDictionary valueForKey:@"data"] valueForKey:@"refferal_code"];
            
            [[NSUserDefaults standardUserDefaults] setValue:app.UserId forKey:@"userid"];
            [[NSUserDefaults standardUserDefaults] setValue:app.User_Full_Name forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setValue:app.User_Email forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] setValue:app.User_Image forKey:@"image"];
            [[NSUserDefaults standardUserDefaults] setValue:app.User_Refferal_Code forKey:@"referalcode"];

            UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
            
            [navigationController pushViewController:Nextview animated:YES];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

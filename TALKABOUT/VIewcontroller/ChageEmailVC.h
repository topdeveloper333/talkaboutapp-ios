//
//  ChageEmailVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface ChageEmailVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UITextField *OldEmailTF;
@property (weak, nonatomic) IBOutlet UITextField *NewEmailTF;
@property (weak, nonatomic) IBOutlet UITextField *ConformEmailTF;
- (IBAction)Save_Btn_Click:(id)sender;
- (IBAction)Back_Btn_Click:(id)sender;

@end

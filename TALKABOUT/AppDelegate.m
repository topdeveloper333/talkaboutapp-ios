//
//  AppDelegate.m
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "AppDelegate.h"
#import "DashboardVC.h"
#import "SignInVC.h"
#import <Sinch/Sinch.h>

@interface AppDelegate ()<SINCallClientDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _BASEURL=@"http://talkaboutapp.co.uk/site-admin/api/";
    NSString *isloginuser=[[NSUserDefaults standardUserDefaults] valueForKey:@"isloginuser"];
    if ([isloginuser isEqualToString:@"yes"])
    {
        _UserId=[[NSUserDefaults standardUserDefaults] valueForKey:@"userid"];
        _User_Full_Name=[[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        _User_Email=[[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
        _User_Image=[[NSUserDefaults standardUserDefaults] valueForKey:@"image"];
        _User_Refferal_Code=[[NSUserDefaults standardUserDefaults] valueForKey:@"referalcode"];
        
        [[NSUserDefaults standardUserDefaults] setValue:_UserId forKey:@"userid"];
        [[NSUserDefaults standardUserDefaults] setValue:_User_Full_Name forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setValue:_User_Email forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] setValue:_User_Image forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] setValue:_User_Refferal_Code forKey:@"referalcode"];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDidLoginNotification"
                                                          object:nil
                                                           queue:nil
                                                      usingBlock:^(NSNotification *note) {
                                                          [self initSinchClientWithUserId:_UserId];
                                                      }];
        
        
        [self initSinchClientWithUserId:_UserId];
        
        
        DashboardVC *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DashboardVC"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        [navController setNavigationBarHidden:YES];
        self.window.rootViewController = navController;
        
    }
    else
    {
        SignInVC *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInVC"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        [navController setNavigationBarHidden:YES];
        self.window.rootViewController = navController;    }
    return YES;
}

- (void)initSinchClientWithUserId:(NSString *)userId
{
    
   
    if (!_client)
    {
        _client = [Sinch clientWithApplicationKey:@"0f39f1e7-4a18-4ca4-b63f-004e96afb068"
                                applicationSecret:@"H2H4ttebsUaT7bzWJswCMA=="
                                  environmentHost:@"clientapi.sinch.com"
                                           userId:userId];
        _client.delegate = self;
        _client.callClient.delegate=self;
        [_client setSupportCalling:YES];
        [_client setSupportActiveConnectionInBackground:YES];
        
        [_client start];
        [_client startListeningOnActiveConnection];
        NSLog(@"CALL START");
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end

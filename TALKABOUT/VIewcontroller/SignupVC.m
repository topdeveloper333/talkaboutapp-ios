//
//  SignupVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 28/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "SignupVC.h"
#import "AppDelegate.h"
#import "DashboardVC.h"
#import "SignInVC.h"
@interface SignupVC ()<UITextFieldDelegate>
{
    AppDelegate *app;
}
@end

@implementation SignupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    

    _Email_Tf.delegate=self;
    _Email_Tf.backgroundColor=[UIColor clearColor];
    _Email_Tf.layer.cornerRadius=5.0f;
    _Email_Tf.layer.borderWidth=1.0f;
    _Email_Tf.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _Email_Tf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _Email_Tf.clipsToBounds=YES;
    
    _Full_name_TF.delegate=self;
    _Full_name_TF.backgroundColor=[UIColor clearColor];
    _Full_name_TF.layer.cornerRadius=5.0f;
    _Full_name_TF.layer.borderWidth=1.0f;
    _Full_name_TF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _Full_name_TF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _Full_name_TF.clipsToBounds=YES;
    
    _pswd_TF.delegate=self;
    _pswd_TF.backgroundColor=[UIColor clearColor];
    _pswd_TF.layer.cornerRadius=5.0f;
    _pswd_TF.layer.borderWidth=1.0f;
    _pswd_TF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _pswd_TF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _pswd_TF.clipsToBounds=YES;
    
    _c_pswd_TF.delegate=self;
    _c_pswd_TF.backgroundColor=[UIColor clearColor];
    _c_pswd_TF.layer.cornerRadius=5.0f;
    _c_pswd_TF.layer.borderWidth=1.0f;
    _c_pswd_TF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _c_pswd_TF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _c_pswd_TF.clipsToBounds=YES;
    
    _R_Code_TF.delegate=self;
    _R_Code_TF.backgroundColor=[UIColor clearColor];
    _R_Code_TF.layer.cornerRadius=5.0f;
    _R_Code_TF.layer.borderWidth=1.0f;
    _R_Code_TF.layer.borderColor=[UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0f].CGColor;
    _R_Code_TF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _R_Code_TF.clipsToBounds=YES;
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}
- (IBAction)SignUp_Btn_Click:(id)sender
{
    if([_Full_name_TF.text isEqualToString:@""])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Warring !" message:@"Enter Full Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if (![self validEmail:_Email_Tf.text ])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter Proper Email id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if([_pswd_TF.text isEqualToString:@""])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if([_c_pswd_TF.text isEqualToString:@""])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter confirm password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if(![_c_pswd_TF.text isEqualToString:_pswd_TF.text])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Mismatch your password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }
    else if([_R_Code_TF.text isEqualToString:@""])
    {
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Error !" message:@"Enter refferal code" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
    }

    else
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(signup_api_call) userInfo:nil repeats:NO];
        
    }
}

- (IBAction)Sign_In_Btn_Click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)Back_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)signup_api_call
{

    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(signup_api_call) userInfo:nil repeats:NO];
                                       
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                   actionWithTitle:@"Cancle"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                      
                                       
                                   }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];

        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@register.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"full_name\":\"%@\",\"email\":\"%@\",\"password\":\"%@\",\"confirm_password\":\"%@\",\"img\":\"%@\",\"refferal_code\":\"%@\"}",_Full_name_TF.text,_Email_Tf.text,_pswd_TF.text,_c_pswd_TF.text,@"img",_R_Code_TF.text];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");

        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            SignInVC *Nextview = (SignInVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"SignInVC"];
            
            [navigationController pushViewController:Nextview animated:YES];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

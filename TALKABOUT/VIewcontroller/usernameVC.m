//
//  usernameVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "usernameVC.h"

@interface usernameVC ()<UITextFieldDelegate,SINCallClientDelegate>

@end

@implementation usernameVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _First_name_TF.backgroundColor=[UIColor clearColor];
    _Last_Name_TF.backgroundColor=[UIColor clearColor];
    
    _First_name_TF.delegate=self;
    _Last_Name_TF.delegate=self;
    
    _First_name_TF.layer.cornerRadius=10;
    _First_name_TF.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _First_name_TF.layer.borderWidth=1.0;
    _First_name_TF.clipsToBounds=YES;
    
    _Last_Name_TF.layer.cornerRadius=10;
    _Last_Name_TF.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _Last_Name_TF.layer.borderWidth=1.0;
    _Last_Name_TF.clipsToBounds=YES;
    
    UIView *FirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_Last_Name_TF setLeftViewMode:UITextFieldViewModeAlways];
    [_Last_Name_TF setLeftView:FirstName];
    
    UIView *FirstName1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_First_name_TF setLeftViewMode:UITextFieldViewModeAlways];
    [_First_name_TF setLeftView:FirstName1];
    
    _popup_back_view.hidden=YES;
    _popup_view.layer.cornerRadius=20.0f;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_btn_Click:(id)sender {
    _popup_back_view.hidden=NO;
}

- (IBAction)save_btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)popup_save_btn_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)popup_dontsave_btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

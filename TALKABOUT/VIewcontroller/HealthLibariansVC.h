//
//  HealthLibariansVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import "MBProgressHUD.h"


@interface HealthLibariansVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;

@property (weak, nonatomic) IBOutlet UIButton *Menu_btn;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;




- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *Hideview;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;

- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;



@end

//
//  JPSThumbnail.h
//  JPSThumbnailAnnotation
//
//  Created by Jean-Pierre Simard on 4/22/13.
//  Copyright (c) 2013 JP Simard. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

typedef void (^ActionBlock)();

@interface JPSThumbnail : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *imageprofile;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) UIImage *star1;
@property (nonatomic, copy) UIImage *star2;
@property (nonatomic, copy) UIImage *star3;
@property (nonatomic, copy) UIImage *star4;
@property (nonatomic, copy) UIImage *star5;
@property (nonatomic, copy) NSString *address;



@property (nonatomic, copy) NSString *subtitle,*msgRcvId,*msgProPic,*msgName,*msContact;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) ActionBlock disclosureBlock;

@end

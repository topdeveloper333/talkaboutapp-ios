//
//  paymentDetailVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface paymentDetailVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;

- (IBAction)back_btn_Click:(id)sender;
- (IBAction)visa_btn_Click:(id)sender;
- (IBAction)mastercard_btn_Click:(id)sender;

- (IBAction)paypal_Btn_Click:(id)sender;
- (IBAction)other_btn_Click:(id)sender;
- (IBAction)save_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *card_no_tf;
@property (weak, nonatomic) IBOutlet UITextField *expiration_tf;
@property (weak, nonatomic) IBOutlet UITextField *Security_code_td;
@property (weak, nonatomic) IBOutlet UITextField *billing_zip_code_tf;

@end

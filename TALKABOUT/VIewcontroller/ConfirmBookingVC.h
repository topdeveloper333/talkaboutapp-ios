//
//  ConfirmBookingVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 01/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"

@interface ConfirmBookingVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UIImageView *img_view;
- (IBAction)place_booking_btn_Click:(id)sender;
- (IBAction)Back_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *main_view;


@property (strong, nonatomic) NSString *DR_ID;
@property (strong, nonatomic) NSString *Booking_type;
@property (strong, nonatomic) NSString *Booking_Time;
@property (strong, nonatomic) NSString *Booking_Date;

@end

//
//  newbookingVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

@interface newbookingVC : UIViewController<JTCalendarDelegate,callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) id<SINClient> client;

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarContentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *Time_tbl_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
- (IBAction)Cancle_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Time_View;
@property (strong, nonatomic) NSString *Booking_type;
@property (weak, nonatomic) IBOutlet UILabel *booking_type_title;
@property (weak, nonatomic) IBOutlet UIImageView *bokking_type_image;
@property (strong, nonatomic) NSString *Booking_doctor_id;

@end

//
//  ChageEmailVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "ChageEmailVC.h"

@interface ChageEmailVC ()<UITextFieldDelegate,SINCallClientDelegate>

@end

@implementation ChageEmailVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _OldEmailTF.backgroundColor=[UIColor clearColor];
    _NewEmailTF.backgroundColor=[UIColor clearColor];
    _ConformEmailTF.backgroundColor=[UIColor clearColor];
    
    _OldEmailTF.delegate=self;
    _NewEmailTF.delegate=self;
    _ConformEmailTF.delegate=self;

    _OldEmailTF.layer.cornerRadius=10;
    _OldEmailTF.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _OldEmailTF.layer.borderWidth=1.0;
    _OldEmailTF.clipsToBounds=YES;
    
    _NewEmailTF.layer.cornerRadius=10;
    _NewEmailTF.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _NewEmailTF.layer.borderWidth=1.0;
    _NewEmailTF.clipsToBounds=YES;
    
    _ConformEmailTF.layer.cornerRadius=10;
    _ConformEmailTF.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _ConformEmailTF.layer.borderWidth=1.0;
    _ConformEmailTF.clipsToBounds=YES;
    
    UIView *FirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_OldEmailTF setLeftViewMode:UITextFieldViewModeAlways];
    [_OldEmailTF setLeftView:FirstName];
    
    UIView *FirstName1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_NewEmailTF setLeftViewMode:UITextFieldViewModeAlways];
    [_NewEmailTF setLeftView:FirstName1];
    
    UIView *FirstName2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_ConformEmailTF setLeftViewMode:UITextFieldViewModeAlways];
    [_ConformEmailTF setLeftView:FirstName2];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (IBAction)Save_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)Back_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end

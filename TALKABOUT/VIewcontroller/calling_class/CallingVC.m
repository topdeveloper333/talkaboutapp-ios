//
//  CallingVC.m
//  DipChat
//
//  Created by Daniel Flury on 10/07/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "CallingVC.h"
#import "CallViewController+UI.h"
#import "AppDelegate.h"
#import <Sinch/Sinch.h>

typedef enum { kSpeakerEnable, kSpeakerDisable } SpeakerStatus;
@interface CallingVC ()<SINCallDelegate>
{
    AppDelegate *app;
    id<SINCall> cal;
    id<SINClient> client;
}
@property (weak, nonatomic)     IBOutlet UIButton                   *speakerBtn;
@property (assign, nonatomic)   SpeakerStatus mySpeakerStauts;
@property (nonatomic) BOOL speaker;
@property (nonatomic) BOOL muted;

@end

@interface CallingVC ()

@end
@implementation CallingVC

- (id<SINAudioController>)audioController
{
    return [[(AppDelegate *)[[UIApplication sharedApplication] delegate] client] audioController];
}

- (void)setCall:(id<SINCall>)call
{
    _call = call;
    _call.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    _speaker = NO;
    _muted = NO;

    self.speakerButton.hidden =YES;
    self.muteButton.hidden=YES;
    if ([self.call direction] == SINCallDirectionIncoming)
    {
        [self setCallStatusText:@""];
        [self showButtons:kButtonsAnswerDecline];

        [[self audioController] startPlayingSoundFile:[self pathForSound:@"incoming.wav"] loop:YES];
    }
    else
    {
        [self setCallStatusText:@"calling..."];
        [self showButtons:kButtonsHangup];
    }


    
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.remoteUsername.text = [self.call remoteUserId];
    //    self.remoteUsername.text = [NSString stringWithFormat:@"%@",app.CallerName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)accept:(id)sender
{
    self.speakerButton.hidden =NO;
    self.muteButton.hidden=NO;
    [[self audioController] stopPlayingSoundFile];
    [self.call answer];
}
- (void)hangup
{
    [_call hangup];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)decline:(id)sender
{
    [self.call hangup];
    [self dismiss];
}
- (IBAction)hangup:(id)sender
{
    [self.call hangup];
    [self dismiss];
}
- (IBAction)speakerButton:(id)sender
{
    if (_speaker) {
        _speaker = NO;
        
        [_speakerButton setBackgroundImage:[UIImage imageNamed:@"speakerNew0"] forState:UIControlStateNormal];
    } else {
        _speaker = YES;
        [_speakerButton setBackgroundImage:[UIImage imageNamed:@"speaker_selected"] forState:UIControlStateNormal];
    }}

- (IBAction)muteButton:(id)sender
{
    if (_muted) {
        _muted = NO;
        [[self audioController] mute];
        [self.delegate unMute];
        [_muteButton setBackgroundImage:[UIImage imageNamed:@"muteNew"] forState:UIControlStateNormal];
    } else {
        _muted = YES;
        [[self audioController] unmute];
        [_muteButton setBackgroundImage:[UIImage imageNamed:@"mute_selected"] forState:UIControlStateNormal];
    }
}
- (IBAction)speakerBtnClicked:(id)sender
{
    if(self.mySpeakerStauts == kSpeakerEnable)
    {
        [[self audioController] disableSpeaker];
        self.mySpeakerStauts = kSpeakerDisable;
        [self.speakerBtn setBackgroundImage:[UIImage imageNamed:@"speaker_selected.png"] forState:UIControlStateNormal];
    }
    else if(self.mySpeakerStauts == kSpeakerDisable)
    {
        [[self audioController] enableSpeaker];
        self.mySpeakerStauts = kSpeakerEnable;
        [self.speakerBtn setBackgroundImage:[UIImage imageNamed:@"Speaker.png"] forState:UIControlStateNormal];
    }
}

- (void)onDurationTimer:(NSTimer *)unused
{
    NSInteger duration = [[NSDate date] timeIntervalSinceDate:[[self.call details] establishedTime]];
    [self setDuration:duration];
}

#pragma mark - SINCallDelegate

- (void)callDidProgress:(id<SINCall>)call
{
    [self setCallStatusText:@"ringing..."];
    [[self audioController] startPlayingSoundFile:[self pathForSound:@"ringback.wav"] loop:YES];
}

- (void)callDidEstablish:(id<SINCall>)call
{
    self.speakerButton.hidden =NO;
    self.muteButton.hidden=NO;
    [self startCallDurationTimerWithSelector:@selector(onDurationTimer:)];
    [self showButtons:kButtonsHangup];
    [[self audioController] stopPlayingSoundFile];
}

- (void)callDidEnd:(id<SINCall>)call
{
    [self dismiss];
    [[self audioController] stopPlayingSoundFile];
    [self stopCallDurationTimer];
}

#pragma mark - Sounds

- (NSString *)pathForSound:(NSString *)soundName
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:soundName];
}
@end

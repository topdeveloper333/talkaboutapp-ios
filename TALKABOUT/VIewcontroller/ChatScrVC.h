//
//  ChatScrVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNCChatMessage.h"
#import "PTSMessagingCell.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface ChatScrVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
- (IBAction)back_btn_click:(id)sender;
- (IBAction)Send_Btn_Click:(id)sender;
- (IBAction)calender_btn_Click:(id)sender;
- (IBAction)camera_btn_Click:(id)sender;
- (IBAction)imog_btn_Click:(id)sender;
- (IBAction)language_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *msgTF;
@property (weak, nonatomic) IBOutlet UIView *emojiview;
@property (weak, nonatomic) IBOutlet UIView *bottomVIew;
@property (weak, nonatomic) IBOutlet UITableView *chatTblview;

@end

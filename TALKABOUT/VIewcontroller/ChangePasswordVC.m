//
//  ChangePasswordVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "Reachability.h"
#import "AppDelegate.h"
@interface ChangePasswordVC ()<UITextFieldDelegate,SINCallClientDelegate>
{
    AppDelegate *app;
}
@end

@implementation ChangePasswordVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    _old_pswd_tf.backgroundColor=[UIColor clearColor];
    _newpswd_tf.backgroundColor=[UIColor clearColor];
    _con_pswd_tf.backgroundColor=[UIColor clearColor];
    
    _old_pswd_tf.delegate=self;
    _newpswd_tf.delegate=self;
    _con_pswd_tf.delegate=self;
    
    _old_pswd_tf.layer.cornerRadius=10;
    _old_pswd_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _old_pswd_tf.layer.borderWidth=1.0;
    _old_pswd_tf.clipsToBounds=YES;
    
    _newpswd_tf.layer.cornerRadius=10;
    _newpswd_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _newpswd_tf.layer.borderWidth=1.0;
    _newpswd_tf.clipsToBounds=YES;
    
    _con_pswd_tf.layer.cornerRadius=10;
    _con_pswd_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _con_pswd_tf.layer.borderWidth=1.0;
    _con_pswd_tf.clipsToBounds=YES;
    
    UIView *FirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_old_pswd_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_old_pswd_tf setLeftView:FirstName];
    
    UIView *FirstName1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_newpswd_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_newpswd_tf setLeftView:FirstName1];
    
    UIView *FirstName2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_con_pswd_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_con_pswd_tf setLeftView:FirstName2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Back_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)save_btn_Click:(id)sender {
    
    if([_old_pswd_tf.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Password"
                                     message:@"Fill old password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if([_newpswd_tf.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Password"
                                     message:@"Fill new password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if([_con_pswd_tf.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Password"
                                     message:@"Fill conform password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(change_password_api) userInfo:nil repeats:NO];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
-(void)change_password_api
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(change_password_api) userInfo:nil repeats:NO];
                                       
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@change-password.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"user_id\":\"%@\",\"old_password\":\"%@\",\"new_password\":\"%@\",\"confirm_password\":\"%@\"}",app.UserId,_old_pswd_tf.text,_newpswd_tf.text,_con_pswd_tf.text];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           [self.navigationController popViewControllerAnimated:YES];

                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];

        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

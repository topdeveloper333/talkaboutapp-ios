//
//  HealthProfesserDetailVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 01/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"



@interface HealthProfesserDetailVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UILabel *Doctor_name;
@property (weak, nonatomic) IBOutlet UILabel *Doctor_type;
@property (weak, nonatomic) IBOutlet UIButton *add_review_btn;
- (IBAction)add_review_btn_click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *tolk_now_btn;
@property (weak, nonatomic) IBOutlet UIButton *book_now_btn;

- (IBAction)back_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *profile_Img_view;
@property (weak, nonatomic) IBOutlet UIImageView *online_dot_view;
@property (weak, nonatomic) IBOutlet UIImageView *star1;
@property (weak, nonatomic) IBOutlet UIImageView *star2;
@property (weak, nonatomic) IBOutlet UIImageView *star3;
@property (weak, nonatomic) IBOutlet UIImageView *star4;
@property (weak, nonatomic) IBOutlet UIImageView *star5;
- (IBAction)add_to_fav_btn_Click:(id)sender;
- (IBAction)VIew_on_map_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *info_tbl_view;
- (IBAction)Book_noe_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *popup_view;
- (IBAction)popup_close_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *popupview1;
@property (weak, nonatomic) IBOutlet UIView *popupview2;
@property (weak, nonatomic) IBOutlet UIView *popupview3;
@property (weak, nonatomic) IBOutlet UIView *popupview4;
- (IBAction)popup_f2f_btn_Click:(id)sender;
- (IBAction)popup_Phone_btn_Click:(id)sender;
- (IBAction)popup_msg_btn_Click:(id)sender;
- (IBAction)popup_video_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *tabbar;


- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;

- (IBAction)Patient_review_Btn_click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Patient_review_Btn;
@property (weak, nonatomic) IBOutlet UILabel *Patient_review_Line;
- (IBAction)information_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *information_btn;
@property (weak, nonatomic) IBOutlet UILabel *information_Line;
- (IBAction)TalkNow_btn_Click:(id)sender;
- (IBAction)View_on_map_btn_Click:(id)sender;

@property (weak, nonatomic) NSString *Doctor_Id;

@property (weak, nonatomic) IBOutlet UIView *review_popup;
@property (weak, nonatomic) IBOutlet UIView *reviewsub_popup;
@property (weak, nonatomic) IBOutlet UIImageView *Popup_star1_img;
@property (weak, nonatomic) IBOutlet UIImageView *Popup_star2_img;
@property (weak, nonatomic) IBOutlet UIImageView *Popup_star3_img;
@property (weak, nonatomic) IBOutlet UIImageView *Popup_star4_img;
@property (weak, nonatomic) IBOutlet UIImageView *Popup_star5_img;

- (IBAction)Popup_star1_Btn_Click:(id)sender;
- (IBAction)Popup_star2_Btn_Click:(id)sender;
- (IBAction)Popup_star3_Btn_Click:(id)sender;
- (IBAction)Popup_star4_Btn_Click:(id)sender;
- (IBAction)Popup_star5_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *popup_name_tf;
- (IBAction)Popup_ok_Btn_Click:(id)sender;
- (IBAction)popup_cancle_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *Popup_review_tv;


@end

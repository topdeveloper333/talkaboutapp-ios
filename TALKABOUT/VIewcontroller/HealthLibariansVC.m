//
//  HealthLibariansVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "HealthLibariansVC.h"
#import "DashboardVC.h"
#import "AppDelegate.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "SideViewVC.h"
#import "Health_library_detailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
 
@interface HealthLibariansVC ()<SINCallClientDelegate>
{
    CGRect menu_btn_frm;
    NSMutableArray *Collectionviewdata;
        AppDelegate *app;
    
}
@end

@implementation HealthLibariansVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_alldata) userInfo:nil repeats:NO];
   
    

    menu_btn_frm=CGRectMake(_Menu_btn.frame.origin.x, _Menu_btn.frame.origin.y, _Menu_btn.frame.size.width, _Menu_btn.frame.size.height);

    _Tabbar_View.backgroundColor=[UIColor whiteColor];
    [self addshadow:_Tabbar_View];
    
    _Sideview.hidden=YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
    
    sb.view.backgroundColor = [UIColor clearColor];
    [sb willMoveToParentViewController:self];
    
    [self.SubSIdeVIew addSubview:sb.view];
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
    
   
    
}
-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _Menu_btn.frame=menu_btn_frm;
//    [self.Menu_btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return Collectionviewdata.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    UIImageView *backImageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *title = (UILabel *)[cell viewWithTag:101];
    backImageView.layer.cornerRadius=2.0;

    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"http://talkaboutapp.co.uk/site-admin/api/Health/%@",[[Collectionviewdata valueForKey:@"image"] objectAtIndex:indexPath.row]]];
    [backImageView sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@""]];
    
    title.text=[[Collectionviewdata valueForKey:@"title"] objectAtIndex:indexPath.row];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    return CGSizeMake((collectionView.frame.size.width-5)/2, collectionView.frame.size.width/3);
    return CGSizeMake((collectionView.frame.size.width-5)/2, (((collectionView.frame.size.width-5)/2)/2)+7);

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_library_detailVC *Nextview = (Health_library_detailVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_library_detailVC"];
    Nextview.titleText=[[Collectionviewdata valueForKey:@"title"] objectAtIndex:indexPath.row];
    Nextview.Description_text=[[Collectionviewdata valueForKey:@"content"] objectAtIndex:indexPath.row];
    [navigationController pushViewController:Nextview animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)menu_Btn_Click:(id)sender {
    if (_Sideview.hidden==YES) {
        
        _Sideview.hidden=NO;
        self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
        _Menu_btn.frame=CGRectMake(_Menu_btn.frame.origin.x,_Menu_btn.frame.origin.y, 30, 30);
        [self.Menu_btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=YES;
        
        
    }
    else
    {
        _Sideview.hidden=YES;
        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _Menu_btn.frame=menu_btn_frm;
        [self.Menu_btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=NO;
        
        
        
        
    }
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}


- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
-(void)get_alldata
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(get_alldata) userInfo:nil repeats:NO];
                                       
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@health_library.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonServiceCall:URLString];
        
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        Collectionviewdata=[[NSMutableArray alloc] init];
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            Collectionviewdata=[jsonDictionary valueForKey:@"data"];
            [_collectionview reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

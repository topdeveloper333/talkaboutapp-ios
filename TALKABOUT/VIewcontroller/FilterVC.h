//
//  FilterVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 01/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface FilterVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UITextField *what_wrong_tf;
@property (weak, nonatomic) IBOutlet UITextField *location_Tf;
@property (weak, nonatomic) IBOutlet UITextField *therapy_tf;
@property (weak, nonatomic) IBOutlet UITextField *takes_tf;
 - (IBAction)applay_filter_btn_Click:(id)sender;
- (IBAction)Close_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *optionview;
@property (weak, nonatomic) IBOutlet UIView *option_sub_view;
- (IBAction)cancle_btn_click:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

- (IBAction)What_wrong_btn_Click:(id)sender;
- (IBAction)location_btn_Click:(id)sender;
- (IBAction)therapy_type_btn_Click:(id)sender;
- (IBAction)takes_btn_Click:(id)sender;


@end

//
//  paymentDetailVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "paymentDetailVC.h"

@interface paymentDetailVC ()<UITextFieldDelegate,SINCallClientDelegate>

@end

@implementation paymentDetailVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _card_no_tf.backgroundColor=[UIColor clearColor];
    _expiration_tf.backgroundColor=[UIColor clearColor];
    _Security_code_td.backgroundColor=[UIColor clearColor];
    _billing_zip_code_tf.backgroundColor=[UIColor clearColor];

    _card_no_tf.delegate=self;
    _expiration_tf.delegate=self;
    _Security_code_td.delegate=self;
    _billing_zip_code_tf.delegate=self;
    
    
    _card_no_tf.layer.cornerRadius=10;
    _card_no_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _card_no_tf.layer.borderWidth=1.0;
    _card_no_tf.clipsToBounds=YES;
    
    _expiration_tf.layer.cornerRadius=10;
    _expiration_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _expiration_tf.layer.borderWidth=1.0;
    _expiration_tf.clipsToBounds=YES;
    
    _Security_code_td.layer.cornerRadius=10;
    _Security_code_td.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _Security_code_td.layer.borderWidth=1.0;
    _Security_code_td.clipsToBounds=YES;
    
    _billing_zip_code_tf.layer.cornerRadius=10;
    _billing_zip_code_tf.layer.borderColor=[UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1].CGColor;
    _billing_zip_code_tf.layer.borderWidth=1.0;
    _billing_zip_code_tf.clipsToBounds=YES;
    
    UIView *FirstName = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_card_no_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_card_no_tf setLeftView:FirstName];
    
    UIView *FirstName1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_expiration_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_expiration_tf setLeftView:FirstName1];
    
    UIView *FirstName2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_Security_code_td setLeftViewMode:UITextFieldViewModeAlways];
    [_Security_code_td setLeftView:FirstName2];
    
    UIView *FirstName3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [_billing_zip_code_tf setLeftViewMode:UITextFieldViewModeAlways];
    [_billing_zip_code_tf setLeftView:FirstName3];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)visa_btn_Click:(id)sender {
}

- (IBAction)mastercard_btn_Click:(id)sender {
}

- (IBAction)paypal_Btn_Click:(id)sender {
}

- (IBAction)other_btn_Click:(id)sender {
}

- (IBAction)save_Btn_Click:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}
@end

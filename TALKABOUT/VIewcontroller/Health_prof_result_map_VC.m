//
//  Health_prof_result_map_VC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "Health_prof_result_map_VC.h"
#import "AppDelegate.h"
#import "Health_prof_result_VC.h"
#import "FilterVC.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HealthProfesserDetailVC.h"

@interface Health_prof_result_map_VC ()<SINCallClientDelegate>
{
    NSMutableArray *arrCoordinateStr;
    NSMutableArray *Doctor_Data;
    int tagvalue;
    AppDelegate *app;
}
@end

@implementation Health_prof_result_map_VC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
int i;
- (void)viewDidLoad {
    [super viewDidLoad];

    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    _tabbar.backgroundColor=[UIColor whiteColor];

    tagvalue=0;
    _popup_pro_img.layer.cornerRadius=_popup_pro_img.frame.size.width/2;
    _popup_pro_img.layer.borderColor=[UIColor colorWithRed:0.0 green:.82 blue:.73 alpha:1].CGColor;
    _popup_pro_img.layer.borderWidth=1.0;
    _popup_pro_img.clipsToBounds=YES;
    
    _mapView.delegate=self;
    _mapView.showsUserLocation=YES;
   
    _popup_view.hidden=YES;

    [self getcurrentlocation];
//    [self setpin];
    i=0;
    [self addshadow:_popup_view];
    [self addshadow:_tabbar];

    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_Filter_Doctor) userInfo:nil repeats:NO];
}

-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getcurrentlocation
{
    objLocationManager = [[CLLocationManager alloc] init];
    objLocationManager.delegate = self;
    objLocationManager.distanceFilter = kCLDistanceFilterNone;
    objLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    if ([objLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [objLocationManager requestWhenInUseAuthorization];
    }
    [objLocationManager startUpdatingLocation];
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = userLocation.coordinate.latitude;
    location.longitude = userLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [mapView setRegion:region animated:YES];
}
-(void)setpin
{
    arrCoordinateStr = [[NSMutableArray alloc] init];
    
    [arrCoordinateStr addObject:@"22.296351f, 71.194231"];
    [arrCoordinateStr addObject:@"22.296480f, 71.294231"];
    [arrCoordinateStr addObject:@"22.396480f, 71.394231"];
    [arrCoordinateStr addObject:@"22.496480f, 71.494231"];
    [arrCoordinateStr addObject:@"22.596480f, 71.594231"];
    [arrCoordinateStr addObject:@"22.696480f, 71.694231"];
    
    for(int i = 0; i < Doctor_Data.count; i++)
    {
        
//        NSString* strCoordinate = [[arrCoordinateStr objectAtIndex:i] stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        // convert string into actual latitude and longitude values
//        NSArray *components = [strCoordinate componentsSeparatedByString:@","];
        
//        double latitude = [components[0] doubleValue];
//        double longitude = [components[1] doubleValue];
        NSLog(@"%f",[[[Doctor_Data valueForKey:@"latitude"]objectAtIndex:i] doubleValue]);
        double latitude = [[[Doctor_Data valueForKey:@"latitude"]objectAtIndex:i] doubleValue];
        double longitude = [[[Doctor_Data valueForKey:@"longitude"] objectAtIndex:i]doubleValue];
        CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(latitude, longitude);
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:coord1];
        annotation.title=[NSString stringWithFormat:@"%d",i];
        
        [self.mapView addAnnotation:annotation];
    }
    
}

-(void)setPopupData
{
    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:tagvalue]]];
    [_popup_pro_img sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
    
    _Popup_name.text=[[Doctor_Data valueForKey:@"name"]objectAtIndex:tagvalue];
    _popup_address.text=[[Doctor_Data valueForKey:@"address"]objectAtIndex:tagvalue];
    _popup_distance.text=[[Doctor_Data valueForKey:@"distance"]objectAtIndex:tagvalue];
    
    NSString *rating=[[Doctor_Data valueForKey:@"rating"]objectAtIndex:tagvalue];
    if ([rating isEqualToString:@"1"]) {
        _popup_star1.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"2"]) {
        _popup_star1.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"3"]) {
        _popup_star1.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"4"]) {
        _popup_star1.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"5"]) {
        _popup_star1.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"selected-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"selected-star.png"];
        
    }
    else
    {
        _popup_star1.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        _popup_star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
}
#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    _popup_view.hidden=NO;
    
    tagvalue=view.tag;
    tagvalue=tagvalue-1;

    NSLog(@"tag %ld",(long)view.tag);
    [self setPopupData];
    UIView *mainview=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 30, 50 )];
    mainview.backgroundColor=[UIColor clearColor];
    mainview.clipsToBounds=YES;
    
    UIImageView *back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
    back_img.image=[UIImage imageNamed:@"deactive-pin.png"];
    back_img.contentMode=UIViewContentModeScaleAspectFit;
    [mainview addSubview:back_img];
    
    UIImageView *pro_img=[[UIImageView alloc] initWithFrame:CGRectMake(4,2.5 ,22, 22)];
//    pro_img.image=[UIImage imageNamed:@"123.jpg"];
    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:tagvalue]]];
    [pro_img sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
    pro_img.contentMode=UIViewContentModeScaleAspectFill;
    pro_img.layer.cornerRadius=pro_img.frame.size.width/2;
    pro_img.clipsToBounds=YES;
    [mainview addSubview:pro_img];
    
    [view addSubview:mainview];
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    
    _popup_view.hidden=YES;
    int tagvalueset=view.tag;
    tagvalueset=tagvalueset-1;
    UIView *mainview=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 30, 50 )];
    mainview.backgroundColor=[UIColor clearColor];
    mainview.clipsToBounds=YES;
    
    UIImageView *back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
    back_img.image=[UIImage imageNamed:@"active-pin.png"];
    back_img.contentMode=UIViewContentModeScaleAspectFit;
    [mainview addSubview:back_img];
    
    UIImageView *pro_img=[[UIImageView alloc] initWithFrame:CGRectMake(4,2.5 ,22, 22)];
//    pro_img.image=[UIImage imageNamed:@"123.jpg"];
    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:tagvalueset]]];
    [pro_img sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
    pro_img.contentMode=UIViewContentModeScaleAspectFill;
    pro_img.layer.cornerRadius=pro_img.frame.size.width/2;
    pro_img.clipsToBounds=YES;
    [mainview addSubview:pro_img];
    
    
    [view addSubview:mainview];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView
           viewForAnnotation:(id<MKAnnotation>)annotation
{
    i++;
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    static NSString *reuseId = @"reuseid";
    MKAnnotationView *av = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (av == nil)
    {
        av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId] ;
        
        UIView *mainview=[[UIView alloc]initWithFrame: CGRectMake(0, 0, 30, 50 )];
        mainview.backgroundColor=[UIColor clearColor];
        mainview.clipsToBounds=YES;
        
        UIImageView *back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
        back_img.image=[UIImage imageNamed:@"active-pin.png"];
        back_img.contentMode=UIViewContentModeScaleAspectFit;
        [mainview addSubview:back_img];
        
        int tagvalueset=i;
        tagvalueset=tagvalueset-1;
        
        UIImageView *pro_img=[[UIImageView alloc] initWithFrame:CGRectMake(4,2.5 ,22, 22)];
//        pro_img.image=[UIImage imageNamed:@"123.jpg"];
        NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:tagvalueset]]];
        [pro_img sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
        pro_img.contentMode=UIViewContentModeScaleAspectFill;
        pro_img.layer.cornerRadius=pro_img.frame.size.width/2;
        pro_img.clipsToBounds=YES;
        [mainview addSubview:pro_img];
        
        
        av.tag=i;
        [av addSubview:mainview];
        
        //Following lets the callout still work if you tap on the label...
        av.canShowCallout = NO;
        av.frame = mainview.frame;
    }
    else
    {
        av.annotation = annotation;
    }
    
    
    return av;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)list_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_prof_result_VC *Nextview = (Health_prof_result_VC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_prof_result_VC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)Filter_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    FilterVC *Nextview = (FilterVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"FilterVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)popup_detail_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthProfesserDetailVC *Nextview = (HealthProfesserDetailVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthProfesserDetailVC"];
    Nextview.Doctor_Id=[[Doctor_Data valueForKey:@"doctor_id"] objectAtIndex:tagvalue];
    [navigationController pushViewController:Nextview animated:YES];
}
////////////////////////////////////////////////////////////////////////////////////////////////
//.................................. Api part ................................................//
////////////////////////////////////////////////////////////////////////////////////////////////


-(void)Get_all_Filter_Doctor
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_Filter_Doctor) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@doctor-filter.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"location\":\"%@\",\"type\":\"%@\",\"therapy\":\"%@\",\"specialties\":\"%@\"}",@"",@"",@"",@""];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            Doctor_Data=[[NSMutableArray alloc] init];
            Doctor_Data=[jsonDictionary valueForKey:@"data"];
            [self setpin];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end

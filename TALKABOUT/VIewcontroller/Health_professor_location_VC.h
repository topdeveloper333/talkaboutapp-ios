//
//  Health_professor_location_VC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKAnnotation.h>
#import <MapKit/MapKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"



@interface Health_professor_location_VC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,callScreenDelegate>
{
    CLLocationManager *objLocationManager;


}
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet UIView *Tabview;
@property (weak, nonatomic) IBOutlet UIView *mainview;

@property (weak, nonatomic) IBOutlet UIView *location_popup_View;
@property (weak, nonatomic) IBOutlet UIImageView *popupimage;
- (IBAction)menu_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menu_btn;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;


- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *sideSubview;
@property (strong, nonatomic) NSString *Latitude,*longitude,*popup_image,*popup_dr_name,*popup_dr_type,*popup_dr_star,*popup_dr_address;;



@end

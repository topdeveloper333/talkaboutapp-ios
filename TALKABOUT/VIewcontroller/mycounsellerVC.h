//
//  mycounsellerVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"


@interface mycounsellerVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (weak, nonatomic) IBOutlet UIButton *Menu_Btn;
@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;
@property (strong, nonatomic) id<SINClient> client;



- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *Hideview;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Booked_btn;
@property (weak, nonatomic) IBOutlet UIButton *favorit_btn;
- (IBAction)Favourite_btn_Click:(id)sender;
- (IBAction)booked_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *Booked_line;
@property (weak, nonatomic) IBOutlet UILabel *favourite_line;
@property (weak, nonatomic) IBOutlet UITableView *tableview;


@end

//
//  ProfileVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "ProfileVC.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "AppDelegate.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "SideViewVC.h"

@interface ProfileVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,SINCallClientDelegate>
{
    AppDelegate *app;
    CGRect menu_btn_frm;

}
@end

@implementation ProfileVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    _Profile_Img_VIew.layer.cornerRadius=_Profile_Img_VIew.frame.size.width/2;
    _Profile_Img_VIew.layer.borderWidth=1.0f;
    _Profile_Img_VIew.layer.borderColor=[UIColor whiteColor].CGColor;
    _Profile_Img_VIew.clipsToBounds=YES;
    
    menu_btn_frm=CGRectMake(_Menu_Btn.frame.origin.x, _Menu_Btn.frame.origin.y, _Menu_Btn.frame.size.width, _Menu_Btn.frame.size.height);

    _Tabbar_View.backgroundColor=[UIColor whiteColor];
    [self addshadow:_Tabbar_View];
    
    _ageTF.backgroundColor=[UIColor clearColor];
    _ageTF.layer.cornerRadius=2.0;
    _ageTF.layer.borderColor=[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5].CGColor;
    _ageTF.layer.borderWidth=0.5;
    
    _Sideview.hidden=YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
    
    sb.view.backgroundColor = [UIColor clearColor];
    [sb willMoveToParentViewController:self];
    
    [self.SubSIdeVIew addSubview:sb.view];
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
    
   
    
}

-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _Menu_Btn.frame=menu_btn_frm;
//    [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)Change_btn_Click:(id)sender
{
}
- (IBAction)Change_profile_btn_click:(id)sender
{
    NSString *other1 = @"Take a photo";
    NSString *other2 = @"Choose Existing Photo";
    NSString *cancelTitle = @"Cancle";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1,other2, nil];
    [actionSheet showInView:self.view];
}
- (IBAction)male_btn_click:(id)sender
{
    _male_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _female_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)female_btn_click:(id)sender
{
    _female_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _male_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)good_btn_click:(id)sender
{
    _good_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _average_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _poor_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];

}

- (IBAction)average_btn_click:(id)sender
{
    _average_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _good_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _poor_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)poor_btn_click:(id)sender
{
    _poor_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _average_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _good_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)visit1_Btn_Click:(id)sender
{
    _lees1_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _v125_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _more5_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)visit1t5_btn_click:(id)sender
{
    _v125_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _lees1_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _more5_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)visitmore5_btn_click:(id)sender
{
    _more5_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _lees1_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
    _v125_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)yes_btn_Click:(id)sender
{
    _yes_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _no_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

- (IBAction)No_btn_Click:(id)sender
{
    _no_img.image=[UIImage imageNamed:@"Profile_pro-check.png"];
    _yes_img.image=[UIImage imageNamed:@"Profile_pro-uncheck.png"];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex ;
{
    if(buttonIndex==0)
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    else if(buttonIndex==1)
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.sourceType= UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _Profile_Img_VIew.image = nil;
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if(!image)image=info[UIImagePickerControllerOriginalImage];
    NSLog(@"Scaling photo");
    _Profile_Img_VIew.image  = image;
    NSData *imagedata = UIImageJPEGRepresentation(_Profile_Img_VIew.image, 0.25);
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)menu_Btn_Click:(id)sender {
    if (_Sideview.hidden==YES) {
        
        _Sideview.hidden=NO;
        self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
        _Menu_Btn.frame=CGRectMake(_Menu_Btn.frame.origin.x,_Menu_Btn.frame.origin.y, 30, 30);
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=YES;
        
        
    }
    else
    {
        _Sideview.hidden=YES;
        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _Menu_Btn.frame=menu_btn_frm;
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=NO;
        
        
        
        
    }
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}



- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
@end

//
//  JPSThumbnailAnnotationView.m
//  JPSThumbnailAnnotationView
//
//  Created by Jean-Pierre Simard on 4/21/13.
//  Copyright (c) 2013 JP Simard. All rights reserved.
//

@import QuartzCore;
#import "JPSThumbnailAnnotationView.h"
#import "JPSThumbnail.h"
//#import "MessageVC.h"
//#import "HomeVC.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "AsyncImageView.h"

NSString * const kJPSThumbnailAnnotationViewReuseID = @"JPSThumbnailAnnotationView";

static CGFloat const kJPSThumbnailAnnotationViewStandardWidth     = 75.0f;
static CGFloat const kJPSThumbnailAnnotationViewStandardHeight    = 87.0f;
static CGFloat const kJPSThumbnailAnnotationViewExpandOffset      = 200.0f;
static CGFloat const kJPSThumbnailAnnotationViewVerticalOffset    = 34.0f;
static CGFloat const kJPSThumbnailAnnotationViewAnimationDuration = 0.25f;

//static CGFloat const kJPSThumbnailAnnotationViewStandardWidth     = 50.0f;
//static CGFloat const kJPSThumbnailAnnotationViewStandardHeight    = 58.0f;
//static CGFloat const kJPSThumbnailAnnotationViewExpandOffset      = 133.0f;
//static CGFloat const kJPSThumbnailAnnotationViewVerticalOffset    = 22.0f;
//static CGFloat const kJPSThumbnailAnnotationViewAnimationDuration = 0.25f;

@interface JPSThumbnailAnnotationView ()<MFMessageComposeViewControllerDelegate>
{
    AppDelegate *app;
    NSString *ForgotPassEmailStr;
//    HomeVC *home;
}

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIImageView *starimg1;
@property (nonatomic, strong) UIImageView *starimg2;
@property (nonatomic, strong) UIImageView *starimg3;
@property (nonatomic, strong) UIImageView *starimg4;
@property (nonatomic, strong) UIImageView *starimg5;
@property (nonatomic, strong) UILabel *addresslbl;

@property (nonatomic, strong) UIButton *cancle,*Chat,*Alert;
@property (nonatomic, strong) ActionBlock disclosureBlock;
@property (nonatomic, strong) NSString *Tag,*msgRcvId,*msgProPic,*msgName,*msgContact;
@property (nonatomic, strong) CAShapeLayer *bgLayer;
@property (nonatomic, strong) UIButton *disclosureButton;
@property (nonatomic, assign) JPSThumbnailAnnotationViewState state;
@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) MKMapView *viewMap;


@property (nonatomic, strong) UIImage *proimg;
@property (nonatomic, strong) UIImage *pinimg;



@end

@implementation JPSThumbnailAnnotationView

#pragma mark - Setup

- (id)initWithAnnotation:(id<MKAnnotation>)annotation
{
    

    self = [super initWithAnnotation:annotation reuseIdentifier:kJPSThumbnailAnnotationViewReuseID];
    
    if (self) {
        self.canShowCallout = NO;
//        self.image=[UIImage imageNamed:@"Annotation"];

        self.frame = CGRectMake(0, 0, -kJPSThumbnailAnnotationViewStandardWidth, kJPSThumbnailAnnotationViewStandardHeight);
        self.backgroundColor = [UIColor clearColor];
        self.centerOffset = CGPointMake(0, -kJPSThumbnailAnnotationViewVerticalOffset);
        
        _state = JPSThumbnailAnnotationViewStateCollapsed;
        
        [self setupView];
    }
    return self;
}

- (void)setupView
{
    app = (AppDelegate*)[[UIApplication sharedApplication]delegate];


    [self setupTitleLabel];
    [self setupSubtitleLabel];
    [self setupstar1];
    [self setupstar2];
    [self setupstar3];
    [self setupstar4];
    [self setupstar5];
    [self setupaddressLabel];
//    [self SetAddbtn];
//    [self SetAlertbtn];
    [self setLayerProperties];
//    [self setButton];
    [self setupImageView];
    [self setDetailGroupAlpha:0.0f];
}

- (void)setButton
{
    _cancle = [[UIButton alloc] initWithFrame:CGRectMake(140.0f, 12.0f, 25.0f, 25.0f)];

    [_cancle setBackgroundImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
    _cancle.hidden=YES;
    _cancle.tag=1;
        _Tag=[NSString stringWithFormat:@"1"];
    [_cancle addTarget:self action:@selector(didTapDisclosureButton) forControlEvents:UIControlEventTouchDown];
//    [self addSubview:_cancle];
   
}

-(void)SetAddbtn
{
    _Chat = [[UIButton alloc] initWithFrame:CGRectMake(-15.0f, _subtitleLabel.frame.size.height+55, 30.0f, 25.0f)];
    [_Chat setBackgroundImage:[UIImage imageNamed:@"message.png"] forState:UIControlStateNormal];
    [_Chat setTitle:@"a" forState:UIControlStateNormal];
    _Chat.hidden=YES;
    _Chat.tag=2;
    _Tag=[NSString stringWithFormat:@"2"];
    [_Chat addTarget:self action:@selector(ChatViewTranfer) forControlEvents:UIControlEventTouchDown];
    [self addSubview:_Chat];
}

-(void)SetAlertbtn
{
    _Alert = [[UIButton alloc] initWithFrame:CGRectMake(_Chat.frame.size.width + 50.0f, _subtitleLabel.frame.size.height+55, 25.0f, 25.0f)];
    [_Alert setBackgroundImage:[UIImage imageNamed:@"help-alert.png"] forState:UIControlStateNormal];
    [_Alert setTitle:@"a" forState:UIControlStateNormal];
    _Alert.hidden=YES;
    _Alert.tag=2;
    _Tag=[NSString stringWithFormat:@"2"];
    [_Alert addTarget:self action:@selector(AlertViewOpen) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_Alert];
    
}

- (void)setupImageView
{
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12.5f, 12.5f, 50.0f, 50.0f)];

    _imageView.layer.cornerRadius = _imageView.frame.size.width / 2;
    _imageView.clipsToBounds = YES;
    _imageView.contentMode=UIViewContentModeScaleAspectFit;

//    _imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    _imageView.layer.borderWidth = 0.5f;
    [self addSubview:_imageView];
}

- (void)setupTitleLabel
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-30.0f, 20.0f, 168.0f, 20.0f)];
    _titleLabel.textColor = [UIColor darkTextColor];
    _titleLabel.font = [UIFont boldSystemFontOfSize:17];
    _titleLabel.minimumScaleFactor = 0.8f;
    _titleLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:_titleLabel];
}

- (void)setupSubtitleLabel
{
    _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-30.0f, 40.0f, 168.0f, 20.0f)];
    _subtitleLabel.textColor = [UIColor grayColor];
    _subtitleLabel.font = [UIFont systemFontOfSize:10.0f];
    [self addSubview:_subtitleLabel];
}
- (void)setupstar1
{
    _starimg1 = [[UIImageView alloc] initWithFrame:CGRectMake(-30.0f, 60.0f, 15, 15.0f)];
    [self addSubview:_starimg1];
}
- (void)setupstar2
{
    _starimg2 = [[UIImageView alloc] initWithFrame:CGRectMake(-5.0f, 60.0f, 15, 15.0f)];
    [self addSubview:_starimg2];
}
- (void)setupstar3
{
    _starimg3 = [[UIImageView alloc] initWithFrame:CGRectMake(20.0f, 60.0f, 15, 15.0f)];
    [self addSubview:_starimg3];
}
- (void)setupstar4
{
    _starimg4 = [[UIImageView alloc] initWithFrame:CGRectMake(45.0f, 60.0f, 15, 15.0f)];
    [self addSubview:_starimg4];
}
- (void)setupstar5
{
    _starimg5 = [[UIImageView alloc] initWithFrame:CGRectMake(70.0f, 60.0f, 15, 15.0f)];
    [self addSubview:_starimg5];
}
- (void)setupaddressLabel
{
    _addresslbl = [[UILabel alloc] initWithFrame:CGRectMake(-30.0f, 82, 168.0f, 30.0f)];
    _addresslbl.textColor = [UIColor grayColor];
    _addresslbl.font = [UIFont systemFontOfSize:12.0f];
    _addresslbl.numberOfLines=0;
    [self addSubview:_addresslbl];
}
- (void)setLayerProperties
{
    _bgLayer = [CAShapeLayer layer];
    CGPathRef path = [self newBubbleWithRect:self.bounds];
    _bgLayer.path = path;
    CFRelease(path);
    _bgLayer.fillColor = [UIColor clearColor].CGColor;
    _bgLayer.shadowColor = [UIColor clearColor].CGColor;
    _bgLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    _bgLayer.shadowRadius = 2.0f;
    _bgLayer.shadowOpacity = 0.5f;
    _bgLayer.masksToBounds = NO;
    [self.layer insertSublayer:_bgLayer atIndex:0];
}

#pragma mark - Updating

- (void)updateWithThumbnail:(JPSThumbnail *)thumbnail
{
    NSLog(@"thmb : %@",thumbnail.title);
    self.coordinate = thumbnail.coordinate;
    self.titleLabel.text = thumbnail.title;
    self.subtitleLabel.text = thumbnail.subtitle;
    self.starimg1.image=thumbnail.star1;
    self.starimg2.image=thumbnail.star2;
    self.starimg3.image=thumbnail.star3;
    self.starimg4.image=thumbnail.star4;
    self.starimg5.image=thumbnail.star5;

    self.addresslbl.text = thumbnail.address;

    self.imageView.image = thumbnail.image;
    self.disclosureBlock = thumbnail.disclosureBlock;
    self.msgName = thumbnail.msgName;
    self.msgProPic = thumbnail.msgProPic;
    self.msgRcvId = thumbnail.msgRcvId;
    
    _proimg=thumbnail.imageprofile;
    _pinimg=thumbnail.image;
    
    
   
//   _imageView.imageURL = [NSURL URLWithString:self.msgProPic];

}

#pragma mark - JPSThumbnailAnnotationViewProtocol

- (void)didSelectAnnotationViewInMap:(MKMapView *)mapView
{
    self.cancle.hidden=NO;
    _Chat.hidden=NO;
    _Alert.hidden=NO;
    
    [mapView setCenterCoordinate:self.coordinate animated:YES];
    
    _viewMap = mapView;
    [self expand];
}

//-(void)CancleBtnPressed
//{
//    _view.hidden=YES;
//}
- (void)didDeselectAnnotationViewInMap:(MKMapView *)mapView
{
    NSLog(@"map : %@",mapView);
    _cancle.hidden=YES;
    _Chat.hidden=YES;
    _Alert.hidden=YES;
//    _view.frame = CGRectMake(self.frame.origin.x,
//                            self.frame.origin.y,
//                            self.frame.size.width - kJPSThumbnailAnnotationViewExpandOffset,
//                            self.frame.size.height);
    [self shrink];
}

#pragma mark - Geometry

- (CGPathRef)newBubbleWithRect:(CGRect)rect
{
    CGFloat stroke = 1.0f;
	CGFloat radius = 7.0f;
	CGMutablePathRef path = CGPathCreateMutable();
	CGFloat parentX = rect.origin.x + rect.size.width/2.0f;
	
	// Determine Size
	rect.size.width -= stroke + 14.0f;
	rect.size.height -= stroke + 29.0f;
	rect.origin.x += stroke / 2.0f + 7.0f;
	rect.origin.y += stroke / 2.0f + 7.0f;
    
	// Create Callout Bubble Path
	CGPathMoveToPoint(path, NULL, rect.origin.x, rect.origin.y + radius);
	CGPathAddLineToPoint(path, NULL, rect.origin.x, rect.origin.y + rect.size.height - radius);
	CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + rect.size.height - radius, radius, M_PI, M_PI_2, 1);
	CGPathAddLineToPoint(path, NULL, parentX - 14.0f, rect.origin.y + rect.size.height);
	CGPathAddLineToPoint(path, NULL, parentX, rect.origin.y + rect.size.height + 14.0f);
	CGPathAddLineToPoint(path, NULL, parentX + 14.0f, rect.origin.y + rect.size.height);
	CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height);
	CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height - radius, radius, M_PI_2, 0.0f, 1.0f);
	CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width, rect.origin.y + radius);
	CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + radius, radius, 0.0f, -M_PI_2, 1.0f);
	CGPathAddLineToPoint(path, NULL, rect.origin.x + radius, rect.origin.y);
	CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + radius, radius, -M_PI_2, M_PI, 1.0f);
	CGPathCloseSubpath(path);
    return path;
}

#pragma mark - Animations

- (void)setDetailGroupAlpha:(CGFloat)alpha
{
    self.disclosureButton.alpha = alpha;
    self.titleLabel.alpha = alpha;
    self.subtitleLabel.alpha = alpha;
    self.addresslbl.alpha=alpha;
    self.starimg1.alpha=alpha;
    self.starimg2.alpha=alpha;
    self.starimg3.alpha=alpha;
    self.starimg4.alpha=alpha;
    self.starimg5.alpha=alpha;

}

- (void)expand
{
    self.frame = CGRectMake(0, 0, kJPSThumbnailAnnotationViewStandardWidth, kJPSThumbnailAnnotationViewStandardHeight+50);
    if (self.state != JPSThumbnailAnnotationViewStateCollapsed) return;
    self.state = JPSThumbnailAnnotationViewStateAnimating;
    [self animateBubbleWithDirection:JPSThumbnailAnnotationViewAnimationDirectionGrow];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width+kJPSThumbnailAnnotationViewExpandOffset, self.frame.size.height);
    self.centerOffset = CGPointMake(kJPSThumbnailAnnotationViewExpandOffset/2.0f,-(kJPSThumbnailAnnotationViewVerticalOffset+25));
    [UIView animateWithDuration:kJPSThumbnailAnnotationViewAnimationDuration/2.0f delay:kJPSThumbnailAnnotationViewAnimationDuration options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self setDetailGroupAlpha:1.0f];

    } completion:^(BOOL finished)
     {
        self.state = JPSThumbnailAnnotationViewStateExpanded;
     
    }];
}

- (void)shrink
{
    self.frame = CGRectMake(0, 0, kJPSThumbnailAnnotationViewStandardWidth, kJPSThumbnailAnnotationViewStandardHeight);
    if (self.state != JPSThumbnailAnnotationViewStateExpanded) return;
    self.state = JPSThumbnailAnnotationViewStateAnimating;
    self.frame = CGRectMake(self.frame.origin.x,
                            self.frame.origin.y,
                            self.frame.size.width,
                            self.frame.size.height);
    [UIView animateWithDuration:kJPSThumbnailAnnotationViewAnimationDuration/2.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self setDetailGroupAlpha:0.0f];
                     }
                     completion:^(BOOL finished) {
                         
                         [self animateBubbleWithDirection:JPSThumbnailAnnotationViewAnimationDirectionShrink];
                         self.centerOffset = CGPointMake(0.0f, -kJPSThumbnailAnnotationViewVerticalOffset);
                     }];
}
- (void)animateBubbleWithDirection:(JPSThumbnailAnnotationViewAnimationDirection)animationDirection
{
    BOOL growing = (animationDirection == JPSThumbnailAnnotationViewAnimationDirectionGrow);
    
    // Image
    [UIView animateWithDuration:kJPSThumbnailAnnotationViewAnimationDuration animations:^{
        CGFloat xOffset = (growing ? -1 : 1) * kJPSThumbnailAnnotationViewExpandOffset/2.0f;
        
        if (xOffset == -100)
        {
        self.imageView.frame = CGRectOffset(self.imageView.frame, xOffset, 25.0f);
        [_imageView setFrame:CGRectMake(-90.0f, 30.0f, 50.0f, 50.0f)];
            _imageView.image=_proimg;
            _bgLayer.fillColor = [UIColor whiteColor].CGColor;
            _bgLayer.shadowColor = [UIColor blackColor].CGColor;
            _imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            _imageView.layer.borderWidth = 0.5f;
            _imageView.contentMode=UIViewContentModeScaleToFill;

            
        }
        else
        {
            self.imageView.frame = CGRectOffset(self.imageView.frame, xOffset,-25.0f);
            [_imageView setFrame:CGRectMake(12.0f, 12.5f, 50.0f, 50.0f)];
            _imageView.image=_pinimg;
            _bgLayer.fillColor = [UIColor clearColor].CGColor;
            _bgLayer.shadowColor = [UIColor clearColor].CGColor;
            _imageView.contentMode=UIViewContentModeScaleAspectFit;
            _imageView.layer.borderColor = [[UIColor clearColor] CGColor];
            _imageView.layer.borderWidth = 0.5f;
        }
    } completion:^(BOOL finished) {
        if (animationDirection == JPSThumbnailAnnotationViewAnimationDirectionShrink) {
            self.state = JPSThumbnailAnnotationViewStateCollapsed;
        }
    }];
    
    // Bubble
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = 1;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.duration = kJPSThumbnailAnnotationViewAnimationDuration;
    
//     Stroke & Shadow From/To Values
    CGRect largeRect = CGRectInset(self.bounds, -kJPSThumbnailAnnotationViewExpandOffset/2.0f, 0.0f);
    CGPathRef fromPath = [self newBubbleWithRect:growing ? self.bounds : largeRect];
    animation.fromValue = (__bridge id)fromPath;
    CGPathRelease(fromPath);
    CGPathRef toPath = [self newBubbleWithRect:growing ? largeRect : self.bounds];
    animation.toValue = (__bridge id)toPath;
    CGPathRelease(toPath);
    [self.bgLayer addAnimation:animation forKey:animation.keyPath];
}

#pragma mark - Disclosure Button
- (void)didTapDisclosureButton
{
    [self didDeselectAnnotationViewInMap:_viewMap];
//    [self shrink];
    _cancle.hidden=YES;
    _Chat.hidden=YES;
    _Alert.hidden=YES;
}
//- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
//        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
//    }
//}
-(void)ChatViewTranfer
{
//    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    MessageVC *Listroute = (MessageVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageVC"];
//    Listroute.UserName = self.titleLabel.text;
//    Listroute.UserProfile = self.msgProPic;
//    Listroute.ReceiverId = self.msgRcvId;
//    NSLog(@"%@",Listroute.ReceiverId);
//    app.SelectedChatId=self.msgRcvId;
//    [navigationController pushViewController:Listroute animated:YES];
}

-(void)AlertViewOpen
{
//    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    MessageVC *Listroute = (MessageVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MessageVC"];
//    Listroute.UserName = self.titleLabel.text;
//    Listroute.UserProfile = self.msgProPic;
//    Listroute.ReceiverId = self.msgRcvId;
//    NSLog(@"%@",Listroute.ReceiverId);
//    app.SelectedChatId=self.msgRcvId;
//    [navigationController pushViewController:Listroute animated:YES];
////    NSString *Message=[NSString stringWithFormat:@"Send Alert Message to %@",_titleLabel.text];
////    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@""
////                                                     message:Message
////                                                    delegate:self
////                                           cancelButtonTitle:@"Cancel"
////                                           otherButtonTitles:@"Send", nil];
////    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
////    UITextField * alertTextField1 = [alert textFieldAtIndex:0];
////    CGRect finalFrame = alertTextField1.frame;
////    finalFrame.size.height = 35;
////    [alertTextField1 setFrame:finalFrame];
////    alertTextField1.keyboardType = UIKeyboardTypeDefault;
////    alertTextField1.placeholder = @"Message";
////    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"1 %@", [alertView textFieldAtIndex:0].text);
        ForgotPassEmailStr=[alertView textFieldAtIndex:0].text;
    
        if([ForgotPassEmailStr isEqualToString:@""])
        {
            if (buttonIndex == 1)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                                message:@"You need to enter your email address.."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
//            app.messagetextStr = ForgotPassEmailStr;
//            app.contactMessageArr = [self.msgContact copy];
//            home = [[HomeVC alloc]init];
//            [home getMessageCall];
        }
}


+ (UIImage *)disclosureButtonImage
{
    CGSize size = CGSizeMake(21.0f, 36.0f);
    UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(2.0f, 2.0f)];
    [bezierPath addLineToPoint:CGPointMake(10.0f, 10.0f)];
    [bezierPath addLineToPoint:CGPointMake(2.0f, 18.0f)];
    [[UIColor lightGrayColor] setStroke];
    bezierPath.lineWidth = 3.0f;
    [bezierPath stroke];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

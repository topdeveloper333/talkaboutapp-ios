//
//  ChangePasswordVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceNSObject.h"
#import "Reachability.h"
#import <Sinch/Sinch.h>
#import "CallingVC.h"


@interface ChangePasswordVC : UIViewController<callScreenDelegate>
{
    ServiceNSObject *jsonServiceNSObjectCall;
    MBProgressHUD *hud;
    
}
@property (strong, nonatomic) id<SINClient> client;

- (IBAction)Back_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *old_pswd_tf;
@property (weak, nonatomic) IBOutlet UITextField *con_pswd_tf;
- (IBAction)save_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *newpswd_tf;

@end

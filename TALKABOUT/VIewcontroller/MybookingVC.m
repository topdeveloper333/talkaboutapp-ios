//
//  MybookingVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "MybookingVC.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "AppDelegate.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "SideViewVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MybookingVC ()<UITableViewDelegate, UITableViewDataSource,SINCallClientDelegate>
{
    AppDelegate *app;
    CGRect menu_btn_frm;
   
    NSMutableArray *DropDownlist;
    NSString *selectedDropDown;

}
@end

@implementation MybookingVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    _DropDownVIew.hidden=YES;
    
    _dropdowntable.dataSource=self;
    _dropdowntable.delegate=self;

    _Tabbar_View.backgroundColor=[UIColor whiteColor];
    [self addshadow:_Tabbar_View];
    menu_btn_frm=CGRectMake(_Menu_Btn.frame.origin.x, _Menu_Btn.frame.origin.y, _Menu_Btn.frame.size.width, _Menu_Btn.frame.size.height);
    
    DropDownlist=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    
    _Sideview.hidden=YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
    
    sb.view.backgroundColor = [UIColor clearColor];
    [sb willMoveToParentViewController:self];
    
    [self.SubSIdeVIew addSubview:sb.view];
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
    
   
}
-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _Menu_Btn.frame=menu_btn_frm;
//    [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _DropDownTableview) {
        return DropDownlist.count;
    }
    return app.Booking_list_array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _DropDownTableview) {
       
        static NSString *simpleTableIdentifier = @"Pickercell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
             }
            cell.textLabel.text=[DropDownlist objectAtIndex:indexPath.row];
            return cell;
       
    }
    
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    UIView *background=(UIView *)[cell viewWithTag:99];
    UIImageView *imageview=(UIImageView *)[cell viewWithTag:100];
    UILabel *name=(UILabel *)[cell viewWithTag:101];
    UILabel *fees=(UILabel *)[cell viewWithTag:102];
    UILabel *type=(UILabel *)[cell viewWithTag:103];
    UILabel *time=(UILabel *)[cell viewWithTag:104];
    UILabel *status=(UILabel *)[cell viewWithTag:105];
    UILabel *date=(UILabel *)[cell viewWithTag:106];

    [self addshadow:background];
    fees.layer.cornerRadius=fees.frame.size.height/2;
    fees.clipsToBounds=YES;
    imageview.layer.cornerRadius=imageview.frame.size.width/2;
    imageview.layer.borderWidth=1.0f;
    imageview.layer.borderColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1].CGColor;
    imageview.clipsToBounds=YES;
    
    NSURL *imageurl=[NSURL URLWithString:[[app.Booking_list_array valueForKey:@"image"] objectAtIndex:indexPath.row]];
    [imageview sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
    date.text=[[app.Booking_list_array valueForKey:@"booking_date"] objectAtIndex:indexPath.row];
    time.text=[[app.Booking_list_array valueForKey:@"booking_time"] objectAtIndex:indexPath.row];
    name.text=[[app.Booking_list_array valueForKey:@"name"] objectAtIndex:indexPath.row];
    status.text=@"Approwed";
    type.text=[[app.Booking_list_array valueForKey:@"type"] objectAtIndex:indexPath.row];
    fees.text=[NSString stringWithFormat:@"$%@",[[app.Booking_list_array valueForKey:@"charge"] objectAtIndex:indexPath.row]];

    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _DropDownTableview) {
        return 40;
    }
    return 95;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _DropDownTableview)
    {
        _DropDownVIew.hidden=YES;
        
        if ([selectedDropDown isEqualToString:@"T"]) {
            _Typer_view_Lbl.text=[DropDownlist objectAtIndex:indexPath.row];
        }
        else  if ([selectedDropDown isEqualToString:@"S"]) {
            _Status_view_Lbl.text=[DropDownlist objectAtIndex:indexPath.row];
        }
        else
        {
            _Date_view_Lbl.text=[DropDownlist objectAtIndex:indexPath.row];
        }
    }
    _DropDownVIew.hidden=YES;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)menu_Btn_Click:(id)sender {
    if (_Sideview.hidden==YES) {
        
        _Sideview.hidden=NO;
        self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
        _Menu_Btn.frame=CGRectMake(_Menu_Btn.frame.origin.x,_Menu_Btn.frame.origin.y, 30, 30);
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=YES;
        
        
    }
    else
    {
        _Sideview.hidden=YES;
        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _Menu_Btn.frame=menu_btn_frm;
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=NO;
        
        
        
        
    }
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}


- (IBAction)typer_btn_click:(id)sender
{
    if (_DropDownVIew.hidden==NO) {
        _DropDownVIew.hidden=YES;
    }
    else
    {
    selectedDropDown=@"T";
    _DropDownVIew.hidden=NO;
     DropDownlist=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    [_DropDownTableview reloadData];
    }
}
- (IBAction)date_btn_Click:(id)sender
{
    if (_DropDownVIew.hidden==NO) {
        _DropDownVIew.hidden=YES;
    }
    else
    {
    selectedDropDown=@"D";
    _DropDownVIew.hidden=NO;
    DropDownlist=[[NSMutableArray alloc] initWithObjects:@"11/11/1111",@"22/22/2222",@"33/33/3333",@"44/44/4444",@"55/55/5555",@"66/66/6666",@"77/77/7777",@"88/88/8888",@"99/99/9999", nil];
    
    [_DropDownTableview reloadData];
    }

}


- (IBAction)ststus_btn_Click:(id)sender
{
    if (_DropDownVIew.hidden==NO) {
        _DropDownVIew.hidden=YES;
    }
    else
    {
    selectedDropDown=@"S";
     DropDownlist=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    _DropDownVIew.hidden=NO;
    
    [_DropDownTableview reloadData];
    }

}

@end

/*
 PTSMessagingCell.m
 
 Copyright (C) 2012 pontius software GmbH
 
 This program is free software: you can redistribute and/or modify
 it under the terms of the Createive Commons (CC BY-SA 3.0) license
*/

#import "PTSMessagingCell.h"

@implementation PTSMessagingCell

//static CGFloat textMarginHorizontal = 12.0f;
//static CGFloat textMarginVertical = 12;
static CGFloat textMarginHorizontal = 12.0f;
static CGFloat textMarginVertical = 12;

static CGFloat messageTextSize = 13.0;
//static CGFloat messageTextSize = 16.0;
//static CGFloat messageTextSize = 12.0;

@synthesize sent, messageLabel, messageView, timeLabel, avatarImageView, balloonView,profileimagebtn,sendImage;

#pragma mark -

#pragma mark Static methods

+(CGFloat)textMarginHorizontal {
    return textMarginHorizontal;
}

+(CGFloat)textMarginVertical {
    return textMarginVertical;
}

+(CGFloat)maxTextWidth {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 250.0f;
    } else {
        return 400.0f;
    }
}

+(CGSize)messageSize:(NSString*)message
{
//    return [message sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:messageTextSize] constrainedToSize:CGSizeMake([PTSMessagingCell maxTextWidth], CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    return [message sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:18] constrainedToSize:CGSizeMake([PTSMessagingCell maxTextWidth], CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];


}

+(UIImage*)balloonImage:(BOOL)sent isimageVisible:(BOOL)Image isSelected:(BOOL)selected {
//    if (sent == YES && selected == YES) {
//        return [[UIImage imageNamed:@"ChatOrange.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//    } else if (sent == YES && selected == NO) {
//        return [[UIImage imageNamed:@"ChatOrange.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//    } else if (sent == NO && selected == YES) {
//        return [[UIImage imageNamed:@"chatBGLight.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//    } else {
//        return [[UIImage imageNamed:@"chatBGLight.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//    }
//    if (Image==YES) {
//        return [UIImage imageNamed:@"Nooo"];
//    }
    if (sent == YES && selected == YES)
    {
        return [[UIImage imageNamed:@"send_bubble.png"]stretchableImageWithLeftCapWidth:21 topCapHeight:40];
    }
    else if (sent == YES && selected == NO)
    {
        return [[UIImage imageNamed:@"send_bubble.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:40];
    }
    else if (sent == NO && selected == YES)
    {
        return [[UIImage imageNamed:@"recive_bubble.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:40];
    }
    else
    {
        return [[UIImage imageNamed:@"recive_bubble.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:40];
       }
    
//     else if (sent == NO && selected == YES) {
//        return [[UIImage imageNamed:@"chattttt.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//     }
//     else
//     {
//        return [[UIImage imageNamed:@"chattttt.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//     }
}

#pragma mark -
#pragma mark Object-Lifecycle/Memory management

-(id)initMessagingCellWithReuseIdentifier:(NSString*)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        /*Selection-Style of the TableViewCell will be 'None' as it implements its own selection-style.*/
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        /*Now the basic view-lements are initialized...*/
        messageView = [[UIView alloc] initWithFrame:CGRectZero];
        messageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        balloonView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        sendImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
        timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        avatarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile-img.png"]];
        avatarImageView.frame=CGRectMake(0, self.balloonView.frame.origin.y, 30, 30);
        avatarImageView.layer.cornerRadius=avatarImageView.frame.size.width/2;
        avatarImageView.clipsToBounds=YES;
        
        profileimagebtn = [[UIButton alloc] init];
        profileimagebtn.frame=CGRectMake(0, self.balloonView.frame.origin.y, 10, 10);
       
        /*Message-Label*/
        self.messageLabel.backgroundColor = [UIColor clearColor];
        self.messageLabel.font = [UIFont fontWithName:@"Roboto" size:12];
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.numberOfLines = 0;
        
        /*Time-Label*/
        self.timeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10];
//        self.timeLabel.font = [UIFont fontWithName:@"Roboto" size:10];

        self.timeLabel.textColor = [UIColor lightGrayColor];
//        self.timeLabel.textColor = [UIColor blackColor];
        self.timeLabel.backgroundColor = [UIColor clearColor];
        
        /*...and adds them to the view.*/
        [self.messageView addSubview: self.balloonView];
        [self.messageView addSubview: self.messageLabel];
        [self.messageView addSubview:self.sendImage];
        
        [self.contentView addSubview: self.timeLabel];
        [self.contentView addSubview: self.messageView];
        [self.contentView addSubview: self.profileimagebtn];
        [self.contentView addSubview: self.avatarImageView];
//        [self.contentView addSubview: self.profileimagebtn];

        
        /*...and a gesture-recognizer, for LongPressure is added to the view.
         */
        
        UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [recognizer setMinimumPressDuration:1.0f];
        [self addGestureRecognizer:recognizer];
    }
    return self;
}

#pragma mark -

#pragma mark Layouting

- (void)layoutSubviews {
    /*This method layouts the TableViewCell. It calculates the frame for the different subviews, to set the layout according to size and orientation.*/
    
    /*Calculates the size of the message. */
    CGSize textSize = [PTSMessagingCell messageSize:self.messageLabel.text];
    
    /*Calculates the size of the timestamp.*/
    CGSize dateSize = [self.timeLabel.text sizeWithFont:self.timeLabel.font forWidth:[PTSMessagingCell maxTextWidth] lineBreakMode:NSLineBreakByClipping];
    
    /*Initializes the different frames , that need to be calculated.*/
    CGRect ballonViewFrame = CGRectZero;
    CGRect messageLabelFrame = CGRectZero;
    CGRect timeLabelFrame = CGRectZero;
    CGRect avatarImageFrame = CGRectZero;
    CGRect imageframe=CGRectZero;
    
    if (self.sent == YES) {
////        timeLabelFrame = CGRectMake(self.frame.size.width - dateSize.width - textMarginHorizontal, 0.0f, dateSize.width, dateSize.height);
////
////        ballonViewFrame = CGRectMake(self.frame.size.width - (textSize.width + 2*textMarginHorizontal), timeLabelFrame.size.height, textSize.width + 2*textMarginHorizontal, textSize.height + 2*textMarginVertical);
////
////        messageLabelFrame = CGRectMake(self.frame.size.width - (textSize.width + textMarginHorizontal),  ballonViewFrame.origin.y + textMarginVertical, textSize.width, textSize.height);
////
////        avatarImageFrame = CGRectMake(5.0f, timeLabelFrame.size.height, 30, 30);
//
        if (self.image==YES) {
            int xset =self.frame.size.width-10;
            ballonViewFrame = CGRectMake(xset - (125 + 2*textMarginHorizontal), 0.0f, 139, 120 + textMarginVertical);

            messageLabelFrame = CGRectMake(self.frame.size.width - (textSize.width + textMarginHorizontal+10),  ballonViewFrame.origin.y + textMarginVertical-3, textSize.width, textSize.height);
            imageframe=CGRectMake(xset - (120 + 2*textMarginHorizontal),textMarginVertical-8,120,120);
            timeLabelFrame = CGRectMake(self.frame.size.width - dateSize.width - textMarginHorizontal, ballonViewFrame.size.height, dateSize.width, dateSize.height);

            avatarImageFrame = CGRectMake(5.0f, timeLabelFrame.size.height, 30, 30);
            self.messageLabel.textColor = [UIColor blackColor];
        }
        
        else
        {
        int xset =self.frame.size.width+7;
            
//        ballonViewFrame=CGRectMake(xset-(textSize.width + 2*textMarginHorizontal+10),0.0f,textSize.width+ (2*textMarginHorizontal)+5,textSize.height+15);

            ballonViewFrame=CGRectMake(xset-(textSize.width + 2*textMarginHorizontal+30),0.0f,textSize.width+ (2*textMarginHorizontal)+10,textSize.height+20);

            
//        messageLabelFrame = CGRectMake(self.frame.size.width - (textSize.width + textMarginHorizontal+4),  ballonViewFrame.origin.y+5/* + textMarginVertical-3*/, textSize.width, textSize.height);
            messageLabelFrame = CGRectMake(self.frame.size.width - (textSize.width + textMarginHorizontal+19),  ballonViewFrame.origin.y+7/* + textMarginVertical-3*/, textSize.width, textSize.height);

        imageframe=CGRectMake(10,10,120,120);
        timeLabelFrame = CGRectMake(self.frame.size.width - dateSize.width - textMarginHorizontal, ballonViewFrame.size.height-5, dateSize.width, dateSize.height);

        avatarImageFrame = CGRectMake(5.0f,0.0f/* timeLabelFrame.size.height*/, 30, 30);
        self.messageLabel.textColor = [UIColor blackColor];
        }
        self.messageLabel.textColor = [UIColor blackColor];

    }
    else
    {
        if (_image==YES) {

            ballonViewFrame = CGRectMake(40,  0.0f, 139/*120+ (2*textMarginHorizontal)*/, 120 + textMarginVertical);


            messageLabelFrame = CGRectMake(textMarginHorizontal+40, ballonViewFrame.origin.y + textMarginVertical-3, textSize.width, textSize.height);
            imageframe=CGRectMake(ballonViewFrame.origin.x+13,textMarginVertical-8,120,120);

            timeLabelFrame = CGRectMake(textMarginHorizontal+35, ballonViewFrame.size.height, dateSize.width, dateSize.height);

//            avatarImageFrame = CGRectMake(5, timeLabelFrame.size.height, 30, 30);
            avatarImageFrame = CGRectMake(5, ballonViewFrame.size.height-40, 30, 30);

            self.messageLabel.textColor = [UIColor colorWithRed:136.0f/255.0f
                                                          green:136.0f/255.0f
                                                           blue:136.0f/255.0f
                                                          alpha:1.0f];
            self.messageLabel.textColor = [UIColor whiteColor];
        }
        else
        {
//        timeLabelFrame = CGRectMake(textMarginHorizontal, 0.0f, dateSize.width, dateSize.height);
//
//        ballonViewFrame = CGRectMake(0.0f, timeLabelFrame.size.height, textSize.width + 2*textMarginHorizontal, textSize.height + 2*textMarginVertical);
//
//        messageLabelFrame = CGRectMake(textMarginHorizontal, ballonViewFrame.origin.y + textMarginVertical, textSize.width, textSize.height);
//
//
//        avatarImageFrame = CGRectMake(5, timeLabelFrame.size.height, 30, 30);

      //---------

//        ballonViewFrame = CGRectMake(40,  0.0f, textSize.width + 2*textMarginHorizontal, textSize.height+3 /*+ 2*textMarginVertical*/);
//        ballonViewFrame=CGRectMake(30,0.0f,textSize.width+ (2*textMarginHorizontal)+15,textSize.height+15);
            ballonViewFrame=CGRectMake(30,0.0f,textSize.width+ (2*textMarginHorizontal)+20,textSize.height+20 );

        
        messageLabelFrame = CGRectMake(textMarginHorizontal+40, ballonViewFrame.origin.y+8, textSize.width, textSize.height);
        imageframe=CGRectMake(10,10,120,120);

        
//        timeLabelFrame = CGRectMake(textMarginHorizontal, ballonViewFrame.size.height, dateSize.width, dateSize.height);
//        timeLabelFrame = CGRectMake(textMarginHorizontal+30,  ballonViewFrame.origin.y + textMarginVertical-3, dateSize.width, dateSize.height);
        timeLabelFrame = CGRectMake(textMarginHorizontal+30,  ballonViewFrame.size.height-5, dateSize.width, dateSize.height);

        
//        avatarImageFrame = CGRectMake(5,5 /*timeLabelFrame.size.height*/, 30, 30);
            avatarImageFrame = CGRectMake(5, ballonViewFrame.size.height-40, 30, 30);

        self.messageLabel.textColor = [UIColor colorWithRed:136.0f/255.0f
                                                      green:136.0f/255.0f
                                                       blue:136.0f/255.0f
                                                      alpha:1.0f];
        self.messageLabel.textColor = [UIColor whiteColor];
        }
        
    }
    [self.balloonView setContentMode:UIViewContentModeScaleToFill];

    self.balloonView.image = [PTSMessagingCell balloonImage:self.sent isimageVisible:self.image isSelected:self.selected];
    
    /*Sets the pre-initialized frames  for the balloonView and messageView.*/
    self.balloonView.frame = ballonViewFrame;
    self.messageLabel.frame = messageLabelFrame;
    self.sendImage.frame= imageframe;
    
            /*If shown (and loaded), sets the frame for the avatarImageView*/
            if (self.avatarImageView.image != nil)
            {
                self.avatarImageView.frame = avatarImageFrame;
                self.profileimagebtn.frame = avatarImageFrame;
            }
    
            /*If there is next for the timeLabel, sets the frame of the timeLabel.*/
    
            if (self.timeLabel.text != nil)
            {
                self.timeLabel.frame = timeLabelFrame;
            }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	/*Selecting a UIMessagingCell will cause its subviews to be re-layouted. This process will not be animated! So handing animated = YES to this method will do nothing.*/
    [super setSelected:selected animated:NO];
    
    [self setNeedsLayout];
    
    /*Furthermore, the cell becomes first responder when selected.*/
            if (selected == YES)
            {
                [self becomeFirstResponder];
            }
            else
            {
                [self resignFirstResponder];
            }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {

}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
	
}

#pragma mark -
#pragma mark UIGestureRecognizer-Handling

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPressRecognizer {
    /*When a LongPress is recognized, the copy-menu will be displayed.*/
//    when a longpress is recognized the copy menu will be displayed
            if (longPressRecognizer.state != UIGestureRecognizerStateBegan)
            {
                return;
            }
    
            if ([self becomeFirstResponder] == NO)
            {
                return;
            }
    
    UIMenuController * menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.balloonView.frame inView:self];
    
    [menu setMenuVisible:YES animated:YES];
}

-(BOOL)canBecomeFirstResponder {
    /*This cell can become first-responder*/
//    this cell can become first responder
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    /*Allows the copy-Action on this cell.*/

            if (action == @selector(copy:))
            {
                return YES;
            }
            else
            {
                return [super canPerformAction:action withSender:sender];
            }
}

-(void)copy:(id)sender {
    /**Copys the messageString to the clipboard.*/
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.messageLabel.text];
}

@end


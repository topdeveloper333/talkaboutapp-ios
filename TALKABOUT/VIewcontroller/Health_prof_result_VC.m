//
//  Health_prof_result_VC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 02/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "Health_prof_result_VC.h"
#import "Health_prof_result_map_VC.h"
#import "FilterVC.h"
#import "AppDelegate.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "HealthProfesserDetailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface Health_prof_result_VC ()<SINCallClientDelegate>
{
    AppDelegate *app;
    NSMutableArray *Doctor_Data;
}
@end

@implementation Health_prof_result_VC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self addshadow:_tabbar];
    _tabbar.backgroundColor=[UIColor whiteColor];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_Filter_Doctor) userInfo:nil repeats:NO];
    
}
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Doctor_Data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidenti = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidenti];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenti];
    }
    
    UIImageView *pro_img=(UIImageView *)[cell viewWithTag:100];
    UILabel *name=(UILabel *)[cell viewWithTag:101];
    UILabel *specialist=(UILabel *)[cell viewWithTag:102];
    UILabel *address=(UILabel *)[cell viewWithTag:108];
    UILabel *Distions=(UILabel *)[cell viewWithTag:110];

    UIImageView *star1=(UIImageView *)[cell viewWithTag:103];
    UIImageView *star2=(UIImageView *)[cell viewWithTag:104];
    UIImageView *star3=(UIImageView *)[cell viewWithTag:105];
    UIImageView *star4=(UIImageView *)[cell viewWithTag:106];
    UIImageView *star5=(UIImageView *)[cell viewWithTag:107];

    pro_img.layer.cornerRadius=pro_img.frame.size.width/2;    
    pro_img.clipsToBounds=YES;
    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:indexPath.row]]];
    [pro_img sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];

    name.text=[[Doctor_Data valueForKey:@"name"]objectAtIndex:indexPath.row];
    specialist.text=[[Doctor_Data valueForKey:@"specialties"]objectAtIndex:indexPath.row];
    address.text=[[Doctor_Data valueForKey:@"address"]objectAtIndex:indexPath.row];
    Distions.text=[[Doctor_Data valueForKey:@"distance"]objectAtIndex:indexPath.row];

    NSString *rating=[[Doctor_Data valueForKey:@"rating"]objectAtIndex:indexPath.row];
    if ([rating isEqualToString:@"1"]) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];

    }
    else if ([rating isEqualToString:@"2"]) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"3"]) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];

        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"4"]) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"selected-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if ([rating isEqualToString:@"5"]) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"selected-star.png"];
        star5.image=[UIImage imageNamed:@"selected-star.png"];
        
    }
    else
    {
        star1.image=[UIImage imageNamed:@"Diselect-star.png"];
        star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthProfesserDetailVC *Nextview = (HealthProfesserDetailVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthProfesserDetailVC"];
    Nextview.Doctor_Id=[[Doctor_Data valueForKey:@"doctor_id"] objectAtIndex:indexPath.row];

    [navigationController pushViewController:Nextview animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)map_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_prof_result_map_VC *Nextview = (Health_prof_result_map_VC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_prof_result_map_VC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)Filter_btn_Click:(id)sender {
 
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    FilterVC *Nextview = (FilterVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"FilterVC"];

    [navigationController pushViewController:Nextview animated:NO];
    
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

////////////////////////////////////////////////////////////////////////////////////////////////
//..................................aii part .................................................//
////////////////////////////////////////////////////////////////////////////////////////////////


-(void)Get_all_Filter_Doctor
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_Filter_Doctor) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@doctor-filter.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"location\":\"%@\",\"type\":\"%@\",\"therapy\":\"%@\",\"specialties\":\"%@\"}",@"",@"",@"",@""];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            Doctor_Data=[[NSMutableArray alloc] init];
            Doctor_Data=[jsonDictionary valueForKey:@"data"];
            [_tableview reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
        [hud hide:YES];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

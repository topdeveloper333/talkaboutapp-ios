//
//  mycounsellerVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 29/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "mycounsellerVC.h"
#import "DashboardVC.h"
#import "HealthLibariansVC.h"
#import "AppDelegate.h"
#import "MybookingVC.h"
#import "ProfileVC.h"
#import "SideViewVC.h"
#import "HealthProfesserDetailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface mycounsellerVC ()<SINCallClientDelegate>
{
    AppDelegate *app;
    CGRect menu_btn_frm;
    NSMutableArray *all_doctor_array;
}
@end

@implementation mycounsellerVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    menu_btn_frm=CGRectMake(_Menu_Btn.frame.origin.x, _Menu_Btn.frame.origin.y, _Menu_Btn.frame.size.width, _Menu_Btn.frame.size.height);

    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_book_doctor) userInfo:nil repeats:NO];
    
    _Tabbar_View.backgroundColor=[UIColor whiteColor];
    [self addshadow:_Tabbar_View];
    _Sideview.hidden=YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideViewVC *sb = (SideViewVC *)[storyboard instantiateViewControllerWithIdentifier:@"SideViewVC"];
    
    sb.view.backgroundColor = [UIColor clearColor];
    [sb willMoveToParentViewController:self];
    
    [self.SubSIdeVIew addSubview:sb.view];
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
    
    [_Booked_btn setTitleColor:[UIColor colorWithRed:12/255.0 green:192/255.0 blue:168/255.0 alpha:1] forState:UIControlStateNormal];
    [_favorit_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    _favourite_line.hidden=YES;
    _Booked_line.hidden=NO;
    
}
-(void)viewDidAppear:(BOOL)animated
{
//    self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    _Menu_Btn.frame=menu_btn_frm;
//    [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
//    _Hader_back_img.hidden=NO;

}

-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return all_doctor_array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UIImageView *imageview=(UIImageView *)[cell viewWithTag:100];
    imageview.layer.cornerRadius=imageview.frame.size.width/2;
    imageview.layer.borderWidth=1.0f;
    imageview.layer.borderColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1].CGColor;
    imageview.clipsToBounds=YES;
    UILabel *docotorname=(UILabel *)[cell viewWithTag:101];
    UILabel *doctorType=(UILabel *)[cell viewWithTag:102];
    UILabel *doctor_add=(UILabel *)[cell viewWithTag:103];

    UIImageView *star1=(UIImageView *)[cell viewWithTag:104];
    UIImageView *star2=(UIImageView *)[cell viewWithTag:105];
    UIImageView *star3=(UIImageView *)[cell viewWithTag:106];
    UIImageView *star4=(UIImageView *)[cell viewWithTag:107];
    UIImageView *star5=(UIImageView *)[cell viewWithTag:108];

    
    docotorname.text=[[all_doctor_array valueForKey:@"doctor_name"] objectAtIndex:indexPath.row];
    doctorType.text=[[all_doctor_array valueForKey:@"doctor_type"]objectAtIndex:indexPath.row];
    doctor_add.text=[[all_doctor_array valueForKey:@"city"]objectAtIndex:indexPath.row];

    NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[all_doctor_array valueForKey:@"doctor_image"]objectAtIndex:indexPath.row]]];
    [imageview sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
    
    NSString *ratingstring=[[all_doctor_array valueForKey:@"rating"]objectAtIndex:indexPath.row];
    int rating=[ratingstring integerValue];
    if (rating ==1) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if (rating == 2) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if (rating == 3) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if (rating == 4) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"selected-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    else if (rating == 5) {
        star1.image=[UIImage imageNamed:@"selected-star.png"];
        star2.image=[UIImage imageNamed:@"selected-star.png"];
        star3.image=[UIImage imageNamed:@"selected-star.png"];
        star4.image=[UIImage imageNamed:@"selected-star.png"];
        star5.image=[UIImage imageNamed:@"selected-star.png"];
        
    }
    else
    {
        star1.image=[UIImage imageNamed:@"Diselect-star.png"];
        star2.image=[UIImage imageNamed:@"Diselect-star.png"];
        star3.image=[UIImage imageNamed:@"Diselect-star.png"];
        star4.image=[UIImage imageNamed:@"Diselect-star.png"];
        star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 114;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthProfesserDetailVC *Nextview = (HealthProfesserDetailVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthProfesserDetailVC"];
    Nextview.Doctor_Id=[[all_doctor_array valueForKey:@"doctor_id"] objectAtIndex:indexPath.row];

    [navigationController pushViewController:Nextview animated:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menu_Btn_Click:(id)sender {
    if (_Sideview.hidden==YES) {
        
        _Sideview.hidden=NO;
        self.mainview.frame=CGRectMake(_Sideview.frame.size.width, 30, self.view.frame.size.width, self.view.frame.size.height-30);
        _Menu_Btn.frame=CGRectMake(_Menu_Btn.frame.origin.x,_Menu_Btn.frame.origin.y, 30, 30);
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"close_white.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=YES;
        
        
    }
    else
    {
        _Sideview.hidden=YES;
        self.mainview.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _Menu_Btn.frame=menu_btn_frm;
        [self.Menu_Btn setBackgroundImage:[UIImage imageNamed:@"menu-btn.png"] forState:UIControlStateNormal];
        _Hader_back_img.hidden=NO;
        
        
        
        
    }
}
- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)Favourite_btn_Click:(id)sender {
    [_favorit_btn setTitleColor:[UIColor colorWithRed:12/255.0 green:192/255.0 blue:168/255.0 alpha:1] forState:UIControlStateNormal];
    [_Booked_btn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0.0 alpha:0.5] forState:UIControlStateNormal];

    _favourite_line.hidden=NO;
    _Booked_line.hidden=YES;
}

- (IBAction)booked_btn_Click:(id)sender {
    [_Booked_btn setTitleColor:[UIColor colorWithRed:12/255.0 green:192/255.0 blue:168/255.0 alpha:1] forState:UIControlStateNormal];
    [_favorit_btn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0.0 alpha:0.5] forState:UIControlStateNormal];

    _favourite_line.hidden=YES;
    _Booked_line.hidden=NO;
}

-(void)Get_all_book_doctor
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_all_book_doctor) userInfo:nil repeats:NO];
                                   }];
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
       
        NSString *URLString =[NSString stringWithFormat:@"%@fav_doctor.php",app.BASEURL];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",app.UserId];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        all_doctor_array=[[NSMutableArray alloc] init];
        if (success == 1)
        {
            all_doctor_array=[jsonDictionary valueForKey:@"posts"];
            [_tableview reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end

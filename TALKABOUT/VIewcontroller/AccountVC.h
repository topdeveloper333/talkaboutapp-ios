//
//  AccountVC.h
//  TALKABOUT
//
//  Created by Daniel Flury on 31/05/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import "CallingVC.h"

@interface AccountVC : UIViewController<callScreenDelegate>
@property (strong, nonatomic) id<SINClient> client;
@property (weak, nonatomic) IBOutlet UIButton *Menu_Btn;
@property (weak, nonatomic) IBOutlet UIView *Tabbar_View;



- (IBAction)menu_Btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *Sideview;
@property (weak, nonatomic) IBOutlet UIView *SubSIdeVIew;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *Hader_back_img;



- (IBAction)Change_Email_Btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *email_btn;
- (IBAction)Change_pswd_Btn_click:(id)sender;
- (IBAction)change_username_Btn_Click:(id)sender;

- (IBAction)tab_5_btn_Click:(id)sender;
- (IBAction)tab_4_btn_Click:(id)sender;
- (IBAction)tab_3_btn_Click:(id)sender;
- (IBAction)tab_2_btn_Click:(id)sender;
- (IBAction)tab_1_btn_Click:(id)sender;



@end

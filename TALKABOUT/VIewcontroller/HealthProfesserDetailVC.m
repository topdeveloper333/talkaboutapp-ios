//
//  HealthProfesserDetailVC.m
//  TALKABOUT
//
//  Created by Daniel Flury on 01/06/18.
//  Copyright © 2018 Ben Fitzgerald. All rights reserved.
//

#import "HealthProfesserDetailVC.h"
#import "newbookingVC.h"
#import "AppDelegate.h"
#import "HealthLibariansVC.h"
#import "MybookingVC.h"
#import "mycounsellerVC.h"
#import "ProfileVC.h"
#import "DashboardVC.h"
#import "ChatScrVC.h"
#import "Health_professor_location_VC.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface HealthProfesserDetailVC ()<UITextViewDelegate,UITextFieldDelegate,SINCallClientDelegate>
{
    AppDelegate *app;
    NSMutableArray *titleArray;
    NSMutableArray *descArray;
    NSMutableArray *Doctor_Data;
    NSString *tablename;
    NSString *rating_value;
    NSMutableArray *review_data;
}


@end

@implementation HealthProfesserDetailVC
- (id<SINClient>)client {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.client.callClient.delegate = self;
}
#pragma mark - SINCallClientDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"callView"]) {
        CallingVC *callViewController = [segue destinationViewController];
        callViewController.call = sender;
    }
    
}
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)calls
{
    [self performSegueWithIdentifier:@"callView" sender:calls];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)calls
{
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [calls remoteUserId]];
    return notification;
}
- (void)mute {
    id<SINAudioController> audio = [_client audioController];
    [audio mute];
}
- (void)unMute {
    id<SINAudioController> audio = [_client audioController];
    [audio unmute];
}
- (void)speaker {
    id<SINAudioController> audio = [_client audioController];
    [audio enableSpeaker];
}
- (void)speakerOff {
    id<SINAudioController> audio = [_client audioController];
    [audio disableSpeaker];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    _profile_Img_view.layer.cornerRadius=_profile_Img_view.frame.size.width/2;
    _profile_Img_view.clipsToBounds=YES;
    
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    rating_value=@"5";
    titleArray=[[NSMutableArray alloc] initWithObjects:@"Education",@"Languages",@"Board Certifications",@"Awards and Publications",@"Specialies", nil];
    descArray=[[NSMutableArray alloc] initWithObjects:@"Medical school- j.j.m. medical college , devangere, bechelor of medical and bachelor surgery \n Medical school- j.j.m. medical college , devangere, bechelor of medical and bachelor surgery",@"English \n spenis",@"America board of internal medical ",@"America board of internal medical ",@"America board of internal medical ", nil];
    _popup_view.hidden=YES;
    
    _tabbar.backgroundColor=[UIColor whiteColor];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_All_review_Api_Call) userInfo:nil repeats:NO];

    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_Doctor_detail) userInfo:nil repeats:NO];
    tablename=@"info";
    [self addshadow:_popupview1];
    [self addshadow:_popupview2];
    [self addshadow:_popupview3];
    [self addshadow:_popupview4];
    [self addshadow:_tabbar];
    [_information_btn setBackgroundColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:161/255.0f alpha:1.0]];
    [_information_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _information_Line.hidden=NO;
    _Patient_review_Line.hidden=YES;
    
    [_Patient_review_Btn setBackgroundColor:[UIColor clearColor]];
    [_Patient_review_Btn setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5] forState:UIControlStateNormal];
    
    _add_review_btn.hidden=YES;
    _tolk_now_btn.hidden=NO;
    _book_now_btn.hidden=NO;
    
    _review_popup.hidden=YES;
    _reviewsub_popup.layer.cornerRadius=10.0;
    _reviewsub_popup.clipsToBounds=YES;
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"selected-star.png"];


    _Popup_review_tv.layer.borderColor=[UIColor blackColor].CGColor;
    _Popup_review_tv.layer.borderWidth=1.0;
    _popup_name_tf.layer.borderColor=[UIColor blackColor].CGColor;
    _popup_name_tf.layer.borderWidth=1.0;
    _popup_name_tf.backgroundColor=[UIColor clearColor];
    _Popup_review_tv.backgroundColor=[UIColor clearColor];
    _Popup_review_tv.delegate=self;
    _popup_name_tf.delegate=self;
    
   
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [self.view endEditing:YES];
    
    return NO;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tablename isEqualToString:@"review"])
    {
        return review_data.count;
    }
    else
    {
    return titleArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tablename isEqualToString:@"review"])
    {
        static NSString *cellidenti = @"reviewcell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidenti];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenti];
        }
        UIImageView *pro_img=(UIImageView *)[cell viewWithTag:200];
        UILabel *name=(UILabel *)[cell viewWithTag:201];
        UILabel *review=(UILabel *)[cell viewWithTag:202];
        UILabel *time=(UILabel *)[cell viewWithTag:208];

        name.text=[[review_data valueForKey:@"name"] objectAtIndex:indexPath.row];
        review.text=[[review_data valueForKey:@"review"] objectAtIndex:indexPath.row];
        time.text=[[review_data valueForKey:@"time"] objectAtIndex:indexPath.row];
        NSString *starvaluestr=[[review_data valueForKey:@"star"] objectAtIndex:indexPath.row];
        int starvalue=[starvaluestr integerValue];
        
        pro_img.layer.cornerRadius=pro_img.frame.size.width/2;
        pro_img.clipsToBounds=YES;
        
        UIImageView *star1=(UIImageView *)[cell viewWithTag:203];
        UIImageView *star2=(UIImageView *)[cell viewWithTag:204];
        UIImageView *star3=(UIImageView *)[cell viewWithTag:205];
        UIImageView *star4=(UIImageView *)[cell viewWithTag:206];
        UIImageView *star5=(UIImageView *)[cell viewWithTag:207];

//        NSString *rating=[[Doctor_Data valueForKey:@"rating"]objectAtIndex:indexPath.row];
        
        if (starvalue == 1) {
            star1.image=[UIImage imageNamed:@"selected-star.png"];
            star2.image=[UIImage imageNamed:@"Diselect-star.png"];
            star3.image=[UIImage imageNamed:@"Diselect-star.png"];
            star4.image=[UIImage imageNamed:@"Diselect-star.png"];
            star5.image=[UIImage imageNamed:@"Diselect-star.png"];

        }
        else if (starvalue == 2) {
            star1.image=[UIImage imageNamed:@"selected-star.png"];
            star2.image=[UIImage imageNamed:@"selected-star.png"];
            star3.image=[UIImage imageNamed:@"Diselect-star.png"];
            star4.image=[UIImage imageNamed:@"Diselect-star.png"];
            star5.image=[UIImage imageNamed:@"Diselect-star.png"];

        }
        else if (starvalue == 3) {
            star1.image=[UIImage imageNamed:@"selected-star.png"];
            star2.image=[UIImage imageNamed:@"selected-star.png"];
            star3.image=[UIImage imageNamed:@"selected-star.png"];
            star4.image=[UIImage imageNamed:@"Diselect-star.png"];
            star5.image=[UIImage imageNamed:@"Diselect-star.png"];

        }
        else if (starvalue == 4) {
            star1.image=[UIImage imageNamed:@"selected-star.png"];
            star2.image=[UIImage imageNamed:@"selected-star.png"];
            star3.image=[UIImage imageNamed:@"selected-star.png"];
            star4.image=[UIImage imageNamed:@"selected-star.png"];
            star5.image=[UIImage imageNamed:@"Diselect-star.png"];

        }
        else if (starvalue == 5) {
            star1.image=[UIImage imageNamed:@"selected-star.png"];
            star2.image=[UIImage imageNamed:@"selected-star.png"];
            star3.image=[UIImage imageNamed:@"selected-star.png"];
            star4.image=[UIImage imageNamed:@"selected-star.png"];
            star5.image=[UIImage imageNamed:@"selected-star.png"];

        }
        else
        {
            star1.image=[UIImage imageNamed:@"Diselect-star.png"];
            star2.image=[UIImage imageNamed:@"Diselect-star.png"];
            star3.image=[UIImage imageNamed:@"Diselect-star.png"];
            star4.image=[UIImage imageNamed:@"Diselect-star.png"];
            star5.image=[UIImage imageNamed:@"Diselect-star.png"];
        
        }
        return cell;
    }
    else
    {
    static NSString *cellidenti = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidenti];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidenti];
    }
    
    
    UILabel *title=(UILabel *)[cell viewWithTag:100];
    UILabel *desc=(UILabel *)[cell viewWithTag:101];
    
    NSString *cellText =[descArray objectAtIndex:indexPath.row];
    UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    CGSize constraintSize = CGSizeMake(tableView.frame.size.width, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    desc.frame=CGRectMake(desc.frame.origin.x, desc.frame.origin.y, desc.frame.size.width, labelSize.height+10);
    
    
    title.text=[titleArray objectAtIndex:indexPath.row];
    desc.text=[descArray objectAtIndex:indexPath.row];
    
    return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tablename isEqualToString:@"review"])
    {
        return 70;
    }
    else
    {
        
        NSString *cellText =[descArray objectAtIndex:indexPath.row];
        UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        CGSize constraintSize = CGSizeMake(tableView.frame.size.width, MAXFLOAT);
        CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];

    return labelSize.height +30;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)addshadow :(UIView *)View
{
    View.layer.shadowRadius  = 1.5f;
    View.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    View.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    View.layer.shadowOpacity = 0.9f;
    View.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(View.bounds, shadowInsets)];
    View.layer.shadowPath    = shadowPath.CGPath;
}
- (IBAction)add_review_btn_click:(id)sender {
    _review_popup.hidden=NO;
}

- (IBAction)back_btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)add_to_fav_btn_Click:(id)sender {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud show:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Add_to_favorit_api_call) userInfo:nil repeats:NO];
}

- (IBAction)VIew_on_map_Btn_Click:(id)sender {
}
- (IBAction)Book_noe_btn_Click:(id)sender {
    _popup_view.hidden=NO;
}
- (IBAction)popup_close_btn_Click:(id)sender {
    _popup_view.hidden=YES;

}
- (IBAction)popup_f2f_btn_Click:(id)sender {
    _popup_view.hidden=YES;

    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    newbookingVC *Nextview = (newbookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"newbookingVC"];
    Nextview.Booking_type=@"FACETOFACE";
    Nextview.Booking_doctor_id=_Doctor_Id;

    [navigationController pushViewController:Nextview animated:YES];

}

- (IBAction)tab_5_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileVC *Nextview = (ProfileVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)tab_4_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    mycounsellerVC *Nextview = (mycounsellerVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"mycounsellerVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_3_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HealthLibariansVC *Nextview = (HealthLibariansVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"HealthLibariansVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_2_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MybookingVC *Nextview = (MybookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"MybookingVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)tab_1_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DashboardVC *Nextview = (DashboardVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}
- (IBAction)Patient_review_Btn_click:(id)sender {
    [_Patient_review_Btn setBackgroundColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:161/255.0f alpha:1.0]];
    [_Patient_review_Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _Patient_review_Line.hidden=NO;
    _information_Line.hidden=YES;

    [_information_btn setBackgroundColor:[UIColor clearColor]];
    [_information_btn setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5] forState:UIControlStateNormal];
    _add_review_btn.hidden=NO;
    _tolk_now_btn.hidden=YES;
    _book_now_btn.hidden=YES;
    tablename=@"review";
    [_info_tbl_view reloadData];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.fillMode = kCAFillModeForwards;
    transition.duration = 0.6;
    transition.subtype = kCATransitionFromTop;
    
    [_info_tbl_view.layer addAnimation:transition forKey:@"UITableViewReloadDataAnimationKey"];
}
- (IBAction)information_btn_Click:(id)sender {
    [_information_btn setBackgroundColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:161/255.0f alpha:1.0]];
    [_information_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _information_Line.hidden=NO;
    _Patient_review_Line.hidden=YES;

    [_Patient_review_Btn setBackgroundColor:[UIColor clearColor]];
    [_Patient_review_Btn setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5] forState:UIControlStateNormal];
    _add_review_btn.hidden=YES;
    _tolk_now_btn.hidden=NO;
    _book_now_btn.hidden=NO;
    tablename=@"info";
    [_info_tbl_view reloadData];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.fillMode = kCAFillModeForwards;
    transition.duration = 0.6;
    transition.subtype = kCATransitionFromTop;
    
    [_info_tbl_view.layer addAnimation:transition forKey:@"UITableViewReloadDataAnimationKey"];
}
- (IBAction)TalkNow_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ChatScrVC *Nextview = (ChatScrVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"ChatScrVC"];
    
    [navigationController pushViewController:Nextview animated:NO];
}

- (IBAction)View_on_map_btn_Click:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    Health_professor_location_VC *Nextview = (Health_professor_location_VC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"Health_professor_location_VC"];
    Nextview.Latitude=[[Doctor_Data valueForKey:@"latitude"] objectAtIndex:0] ;
    Nextview.longitude=[[Doctor_Data valueForKey:@"longitude"]objectAtIndex:0];
    Nextview.popup_dr_name=[[Doctor_Data valueForKey:@"name"]objectAtIndex:0];
    Nextview.popup_image=[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:0]];
    Nextview.popup_dr_type=[[Doctor_Data valueForKey:@"doctor_type"]objectAtIndex:0];
    Nextview.popup_dr_star=[[Doctor_Data valueForKey:@"rating"]objectAtIndex:0];
    Nextview.popup_dr_address=[[Doctor_Data valueForKey:@"city"]objectAtIndex:0];
   
    

    [navigationController pushViewController:Nextview animated:NO];
   
}
- (IBAction)popup_Phone_btn_Click:(id)sender {
    _popup_view.hidden=YES;
    
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    newbookingVC *Nextview = (newbookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"newbookingVC"];
    Nextview.Booking_type=@"AUDIO";
    Nextview.Booking_doctor_id=_Doctor_Id;

    [navigationController pushViewController:Nextview animated:YES];
}

- (IBAction)popup_msg_btn_Click:(id)sender {
    _popup_view.hidden=YES;
    
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    newbookingVC *Nextview = (newbookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"newbookingVC"];
    Nextview.Booking_type=@"MESSAGE";
    Nextview.Booking_doctor_id=_Doctor_Id;

    [navigationController pushViewController:Nextview animated:YES];
}

- (IBAction)popup_video_btn_Click:(id)sender {
    _popup_view.hidden=YES;
    
    UINavigationController *navigationController = (UINavigationController *)app.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    newbookingVC *Nextview = (newbookingVC *)[mainStoryboard instantiateViewControllerWithIdentifier: @"newbookingVC"];
    Nextview.Booking_type=@"VIDEO";
    Nextview.Booking_doctor_id=_Doctor_Id;

    [navigationController pushViewController:Nextview animated:YES];
}

- (IBAction)Popup_star1_Btn_Click:(id)sender {
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    rating_value=@"1";
}
- (IBAction)Popup_star2_Btn_Click:(id)sender {
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    rating_value=@"2";
    
}
- (IBAction)Popup_star3_Btn_Click:(id)sender {
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    rating_value=@"3";
    
}
- (IBAction)Popup_star4_Btn_Click:(id)sender {
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"Diselect-star.png"];
    rating_value=@"4";
    
}
- (IBAction)Popup_star5_Btn_Click:(id)sender {
    _Popup_star1_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star2_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star3_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star4_img.image=[UIImage imageNamed:@"selected-star.png"];
    _Popup_star5_img.image=[UIImage imageNamed:@"selected-star.png"];
    rating_value=@"5";
    
}

- (IBAction)Popup_ok_Btn_Click:(id)sender {
    if ([_popup_name_tf.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Name"
                                     message:@"Enter your name"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if([_Popup_review_tv.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Review"
                                     message:@"Enter your Review"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        _review_popup.hidden=YES;

        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Add_review_Api_Call) userInfo:nil repeats:NO];
    }
}

- (IBAction)popup_cancle_btn_Click:(id)sender {
    _review_popup.hidden=YES;
}
////////////////////////////////////////////////////////////////////////////////////////////////
//..................................aii part .................................................//
////////////////////////////////////////////////////////////////////////////////////////////////


-(void)Get_Doctor_detail
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_Doctor_detail) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@doctor-detail.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"doctor_id\":\"%@\"}",_Doctor_Id];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            Doctor_Data=[[NSMutableArray alloc] init];
            Doctor_Data=[jsonDictionary valueForKey:@"data"];
            descArray=[[NSMutableArray alloc] init];
            [descArray addObject:[[Doctor_Data valueForKey:@"education"]objectAtIndex:0]];
            [descArray addObject:[[Doctor_Data valueForKey:@"language"]objectAtIndex:0]];
            [descArray addObject:[[Doctor_Data valueForKey:@"board_certification"]objectAtIndex:0]];
            [descArray addObject:[[Doctor_Data valueForKey:@"awards"]objectAtIndex:0]];
            [descArray addObject:[[Doctor_Data valueForKey:@"specialties"]objectAtIndex:0]];
            _Doctor_name.text=[[Doctor_Data valueForKey:@"name"]objectAtIndex:0];
//            _Doctor_type.text=[[Doctor_Data valueForKey:@"doctor_type_id"]objectAtIndex:0];

            NSURL *imageurl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[Doctor_Data valueForKey:@"image"]objectAtIndex:0]]];
            [_profile_Img_view sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"Dr_place.png"]];
            NSString *ratingvalue=[[Doctor_Data valueForKey:@"rating"]objectAtIndex:0];
            int rating = [ratingvalue integerValue];
            if (rating == 1) {
                _star1.image=[UIImage imageNamed:@"selected-star.png"];
                _star2.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star3.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star4.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star5.image=[UIImage imageNamed:@"Diselect-star.png"];
                
            }
            else if (rating == 2) {
                _star1.image=[UIImage imageNamed:@"selected-star.png"];
                _star2.image=[UIImage imageNamed:@"selected-star.png"];
                _star3.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star4.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star5.image=[UIImage imageNamed:@"Diselect-star.png"];
                
            }
            else if (rating == 3) {
                _star1.image=[UIImage imageNamed:@"selected-star.png"];
                
                _star2.image=[UIImage imageNamed:@"selected-star.png"];
                _star3.image=[UIImage imageNamed:@"selected-star.png"];
                _star4.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star5.image=[UIImage imageNamed:@"Diselect-star.png"];
                
            }
            else if (rating == 4) {
                _star1.image=[UIImage imageNamed:@"selected-star.png"];
                _star2.image=[UIImage imageNamed:@"selected-star.png"];
                _star3.image=[UIImage imageNamed:@"selected-star.png"];
                _star4.image=[UIImage imageNamed:@"selected-star.png"];
                _star5.image=[UIImage imageNamed:@"Diselect-star.png"];
                
            }
            else if (rating == 5) {
                _star1.image=[UIImage imageNamed:@"selected-star.png"];
                _star2.image=[UIImage imageNamed:@"selected-star.png"];
                _star3.image=[UIImage imageNamed:@"selected-star.png"];
                _star4.image=[UIImage imageNamed:@"selected-star.png"];
                _star5.image=[UIImage imageNamed:@"selected-star.png"];
                
            }
            else
            {
                _star1.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star2.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star3.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star4.image=[UIImage imageNamed:@"Diselect-star.png"];
                _star5.image=[UIImage imageNamed:@"Diselect-star.png"];
                
            }
            [_info_tbl_view reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)Add_to_favorit_api_call
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Add_to_favorit_api_call) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@add-favorites.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"user_id\":\"%@\",\"doctor_id\":\"%@\"}",app.UserId,_Doctor_Id];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"success"
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)Add_review_Api_Call
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Add_review_Api_Call) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@add-review.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"user_id\":\"%@\",\"doctor_id\":\"%@\",\"name\":\"%@\",\"review\":\"%@\",\"star\":\"%@\"}",app.UserId,_Doctor_Id,_popup_name_tf.text,_Popup_review_tv.text,rating_value];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Success"
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
            [_info_tbl_view reloadData];
          
            
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_All_review_Api_Call) userInfo:nil repeats:NO];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)Get_All_review_Api_Call
{
    
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                       [hud show:YES];
                                       
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_All_review_Api_Call) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:canclebtn];
        [alert addAction:OkButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        NSString *URLString =[NSString stringWithFormat:@"%@get-review.php",app.BASEURL];
        NSString *myRequestString = [NSString stringWithFormat:@"{\"doctor_id\":\"%@\"}",_Doctor_Id];
        jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
        NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:myRequestString];
        NSLog(@"===================================================================");
        NSLog(@"%@",URLString);
        NSLog(@"%@",myRequestString);
        NSLog(@"%@",jsonDictionary);
        NSLog(@"===================================================================");
        
        int success = [[jsonDictionary valueForKey:@"success"] integerValue];
        
        if (success == 1)
        {
            review_data=[[NSMutableArray alloc] init];
            review_data=[jsonDictionary valueForKey:@"data"];
            [_info_tbl_view reloadData];
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"message"]]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* OkButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:OkButton];
//            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [hud hide:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
